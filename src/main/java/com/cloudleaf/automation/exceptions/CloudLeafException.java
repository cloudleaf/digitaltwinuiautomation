package com.cloudleaf.automation.exceptions;

public class CloudLeafException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8401309968233479940L;
	
	public static String failureMessage = "";

	public CloudLeafException(String message)
	{
		super(message);  
		
		failureMessage = message;
	}

}
