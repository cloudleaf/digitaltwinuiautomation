package com.cloudleaf.automation.base;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import com.cloudleaf.automation.exceptions.CloudLeafException;
import com.cloudleaf.automation.pages.CloudLeafLoginPage;
import com.cloudleaf.automation.pages.CommonPage;
import com.cloudleaf.automation.pages.DigitalTwinHomePage;
import com.cloudleaf.automation.pages.AssetDetailsPage;
import com.cloudleaf.automation.pages.AssetsPage;
import com.cloudleaf.automation.pages.ManageDigitalTwinPage;
import com.cloudleaf.automation.pages.MappingAdminPage;
import com.cloudleaf.automation.pages.SensorsPage;
import com.cloudleaf.automation.utils.ChartGenerator;
import com.cloudleaf.automation.utils.PageNavigations;
import com.cloudleaf.automation.utils.PropertyFileUtils;
import com.cloudleaf.automation.utils.ReportGenerator;
import com.cloudleaf.automation.utils.RestAPIUtility;
import com.cloudleaf.automation.utils.Utils;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.response.Response;

public class BasePage {

	Logger log;
	public String browserName;
	public String url;
	protected static String osName, digitalTwinUserName, digitalTwinPassword, api_url, cloudos_url, access_token,
			tenant_id, reportHtml, requestBody = null;
	public APIClient client;
	public JSONObject tcStatus;
	public boolean flag;
	DesiredCapabilities dc;
	protected static final String fs = Utils.getFileSeparator();

	protected static WebDriver driver;
	protected static ExtentReports extent;
	protected static ExtentTest test;
	Random rnum;
	protected static String CURRENTDIR = System.getProperty("user.dir");
	protected static CloudLeafLoginPage loginPage;
	protected static DigitalTwinHomePage dtHomePage;
	protected static ManageDigitalTwinPage manageDTPage;
	protected static AssetsPage manageAssetsPage;
	protected static PageNavigations pageNavigations;
	protected static SensorsPage sensorsPage;
	protected static MappingAdminPage mappingAdminPage;
	protected static AssetDetailsPage assetDetailsPage;
	protected static CommonPage commonPage;
	protected static RestAPIUtility restAPIUtility;
	protected static ArrayList<String> assetsList;
	protected static ArrayList<String> sensorsList;
	protected static final String DATE_FORMAT_NOW = "dd-MM-yyyy-hh-mm-ss";
	protected static final String MAINTENANCE_DATE_FORMAT = "M/d/yyyy, hh:mm:ss a";
	protected static SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
	private static String startTime = "";
	private static String endTime = "";
	protected int passCount = 0, failCount = 0;
	public Map<String, Object> data = new HashMap<String, Object>();
	protected static Response response = null;

	private static String MODULENAME = "";
	protected static final String TCPASSED = "PASSED";
	protected static final String TCFAILED = "FAILED";
	protected static final String TCSKIPPED = "SKIPPED";

	private int passedCount1;
	private int failedCount1;
	private int skippedCount1;
	private int totalCount1;

	private int totalpassedCount;
	private int totalfailedCount;
	private int totalskippedCount;
	private int grandTotalCount;

	private static String chartName = "";

	private static ArrayList<String> startTimeList = new ArrayList<String>();
	private static ArrayList<String> endTimeList = new ArrayList<String>();
	private static ArrayList<String> exceptionList = new ArrayList<String>();
	private static ArrayList<String> modulesList = new ArrayList<String>();
	private static ArrayList<String> TCList = new ArrayList<String>();
	private static ArrayList<Integer> passedList = new ArrayList<Integer>();
	private static ArrayList<Integer> failedList = new ArrayList<Integer>();
	private static ArrayList<Integer> skippedList = new ArrayList<Integer>();
	private static ArrayList<Integer> totalList = new ArrayList<Integer>();
	private static ArrayList<String> totalTimeList = new ArrayList<String>();
	private static ArrayList<String> snapShotList = new ArrayList<String>();
	private static ArrayList<String> tcNameList = new ArrayList<String>();
	private static ArrayList<String> exeStatusList = new ArrayList<String>();

	@BeforeClass
	public void testConfiguration() throws Exception {

//		Utils.killChromeDriver();

		if (PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "emptyReportsFolder")
				.equals("true")) {
			File f = new File(Utils.REPORTS_DIR);
			Utils.emptyFolder(f);
		}
		browserName = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "browserName")
				.trim();
		url = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "digitalTwinUrl").trim();
		osName = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "osName").trim();

		digitalTwinUserName = PropertyFileUtils
				.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "digitalTwinUserName").trim();

		digitalTwinPassword = PropertyFileUtils
				.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "digitalTwinPassword").trim();

		api_url = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "api_url");

		cloudos_url = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "cloudos_url");

		access_token = RestAPIUtility.getAccessToken(
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "digitalTwinUserName"),
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE,
						"digitalTwinPassword"));
		tenant_id = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "tenant_id");

		System.out.println("BASE_URL: " + api_url);

		System.out.println("access_token: " + access_token);

		System.out.println("TENANT_ID: " + tenant_id);

		System.out.println("Browser Name : " + browserName);

		System.out.println("APP URL : " + url);

		System.out.println("OS Name : " + osName);

		System.out.println("USER DIR: " + System.getProperty("user.dir"));
		// System.out.println("TEST METHOD NAME: "+test.getTest().getName());
		log = Logger.getLogger(this.getClass().getName());
		startTime = Utils.getCurrentTimeStamp();
		reportHtml = "ExtentReports/" + "DigitalTwinTestReport.html";

		System.out.println("Test Started at: " + startTime);

		extent = new ExtentReports(reportHtml, false);
		extent.addSystemInfo("Execution Started At ", startTime);
		driver = UIHelper.openBrowser(browserName, url, osName);

		// driver = openBrowser("chrome", "https://test.cloudleaf.io");

		// Getting the browser Name and Version
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName1 = caps.getBrowserName();
		String browserVersion = caps.getVersion();
		System.out.println("Browser Name: " + browserName1);
		System.out.println("Browser Version: " + browserVersion);

		// Extent Report Configuration where your report will be saved if you pass true
		// then existing report will be overwritten, if it's false it adds to the
		// existing report
		extent.addSystemInfo("Environment: ", url).addSystemInfo("User Name: ", "Srinivas Nakka")
				.addSystemInfo("Browser Name: ", browserName1).addSystemInfo("Browser Version: ", browserVersion);

		// Not manadatory if you give the blow line then it will read the vales what we
		// give in the config other wise default values will come
		extent.loadConfig(new File(System.getProperty("user.dir") + "/extent-config.xml"));

		client = new APIClient("https://cloudleaf.testrail.io/");
		client.setUser("rallamsetti@cloudleaf.io");
		client.setPassword("Lakshmi12ab$");

		loginPage = GetSingleTonObject.getLoginPageObject();

		dtHomePage = GetSingleTonObject.getDigitalTwinHomePageObject();

		manageAssetsPage = GetSingleTonObject.getManageAssetsPageObject();

		pageNavigations = GetSingleTonObject.getPageNavigationsObject();

		sensorsPage = GetSingleTonObject.getSensorsPageObject();

		commonPage = GetSingleTonObject.getCommonPageObject();

		mappingAdminPage = GetSingleTonObject.getMappingAdminPageObject();

		assetDetailsPage = GetSingleTonObject.getAssetDetailsPageObject();

		restAPIUtility = GetSingleTonObject.getRestAPIUtilityObject();

		assetsList = new ArrayList<String>();

		sensorsList = new ArrayList<String>();

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException, InterruptedException, CloudLeafException {

		String caseID = result.getName().substring(1, result.getName().length());
		
		String s[] = result.getName().split(" ");

		if (result.getStatus() == ITestResult.SUCCESS) {
			passCount++;
			test.log(LogStatus.PASS, "Test Case Passed >>>>> " + result.getName());

			data.put("status_id", new Integer(1));
			data.put("comment",
					"This test Passed and executed in"
							+ url.substring(8, 13).toUpperCase().replaceAll("[^a-zA-Z0-9]", "")
							+ " CLUSTER for the user " + digitalTwinUserName);
			exeStatusList.add("PASSED");

		}

		if (result.getStatus() == ITestResult.FAILURE) {
			failCount++;
			test.log(LogStatus.FAIL, "Test Case Failed >>>>> " + result.getName());
			test.log(LogStatus.FAIL, "Details of Failed Testcase: " + result.getThrowable());
			String snapshotName = UIHelper.captureScreenShot("DT");
			test.log(LogStatus.FAIL, test.addScreenCapture(snapshotName));
			data.put("status_id", new Integer(5));
			data.put("comment",
					"This test Failed and executed in"
							+ url.substring(8, 13).toUpperCase().replaceAll("[^a-zA-Z0-9]", "")
							+ " CLUSTER for the user " + digitalTwinUserName);
			TCList.add(s[0]);
			exeStatusList.add("FAILED");
			exceptionList.add(getModuleName() + "::" + getTestCaseNameFromScript(s[0]) + "::"
					+ CloudLeafException.failureMessage + "::" + snapshotName);
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(LogStatus.SKIP, "Test Case Skipped >>>> " + result.getName());
			data.put("status_id", new Integer(4));
			data.put("comment",
					"This test Skipped and executed in"
							+ url.substring(8, 13).toUpperCase().replaceAll("[^a-zA-Z0-9]", "")
							+ " CLUSTER for the user " + digitalTwinUserName);
			exeStatusList.add("SKIPPED");
			

		}
		tcStatus = (JSONObject) client.sendPost("add_result_for_case/448/" + caseID, data);
		// Ending the test
		extent.endTest(test);

	}

	@AfterClass
	public void tearDown(ITestContext context) throws ParseException, Exception {

		if (assetsList.size() > 0) {
			pageNavigations.navigateToAssetsPage();
			commonPage.waitUntilLoaderIconNotVisible();
			for (int i = 0; i < assetsList.size(); i++) {
				manageAssetsPage.enterSearchText(assetsList.get(i));
				commonPage.waitUntilLoaderIconNotVisible();
				commonPage.getCheckBoxesList().get(1).click();
				if (commonPage.deleteButton.isEnabled()) {
					commonPage.deleteButton.click();
					commonPage.clickConfirmWindowYesButton();
					commonPage.waitUntilLoaderIconNotVisible();
				}

				Assert.assertTrue(commonPage.getBannerMessageContent().contains("Asset Deleted"));
			}
		}

		if (sensorsList.size() > 0) {
			pageNavigations.navigateToSensorsPage();
			commonPage.waitUntilLoaderIconNotVisible();
			for (int i = 0; i < sensorsList.size(); i++) {
				UIHelper.enterCharByChar(commonPage.searchBar, sensorsList.get(i));
				commonPage.waitUntilLoaderIconNotVisible();
				commonPage.getCheckBoxesList().get(1).click();
				if (commonPage.deleteButton.isEnabled()) {
					commonPage.deleteButton.click();
					commonPage.clickConfirmWindowYesButton();
					commonPage.waitUntilLoaderIconNotVisible();
				}
//				Assert.assertTrue(commonPage.getBannerMessageContent().contains("Sensor Deleted"));
			}
		}
		loginPage.logout();
		UIHelper.closeBrowser();

		for (int i = 0; i < context.getAllTestMethods().length; i++) {
			if (context.getAllTestMethods()[i].getCurrentInvocationCount() > 1) {
				if (context.getFailedTests().getResults(context.getAllTestMethods()[i]).size() > 1
						|| context.getPassedTests().getResults(context.getAllTestMethods()[i]).size() == 1) {

					context.getFailedTests().removeResult(context.getAllTestMethods()[i]);
				}
			}
		}

		String className = this.getClass().getName();
		String myText = className.replace(".", "%");
		String s[] = myText.split("%");
		MODULENAME = s[s.length - 1];
		System.out.println("ModuleName: " + MODULENAME);

		for (int i = 0; i < context.getAllTestMethods().length; i++) {
			String s2 = context.getAllTestMethods()[i].getMethodName();
			tcNameList.add(s2);
			System.out.println("MODULENAME: " + MODULENAME + "::TCName: " + s2);

		}

		System.out.println("LIST: " + exeStatusList.size());

		String tcDetails = ReportGenerator.TCwriteToHTML(MODULENAME, tcNameList, exeStatusList);

		modulesList.add(s[s.length - 1]);

		System.out.println("Number of Modules Executed So Far: " + modulesList.size());

		System.out.println("Name of the Modules Executed So Far: ");
		for (int i = 0; i < modulesList.size(); i++) {
			System.out.println(modulesList.get(i));
		}

		passedCount1 = passedCount1 + context.getPassedTests().size();
		failedCount1 = failedCount1 + context.getFailedTests().size();
		skippedCount1 = skippedCount1 + context.getSkippedTests().size();
		totalCount1 = passedCount1 + failedCount1 + skippedCount1;

		System.out.println("PASSED: " + passedCount1);
		System.out.println("FAILED: " + failedCount1);
		System.out.println("SKIPPED: " + skippedCount1);
		System.out.println("TOTAL: " + totalCount1);

		passedList.add(passedCount1);
		failedList.add(failedCount1);
		skippedList.add(skippedCount1);
		totalList.add(totalCount1);

		for (int i = 0; i < passedList.size(); i++) {
			totalpassedCount = totalpassedCount + passedList.get(i);
		}
		for (int i = 0; i < failedList.size(); i++) {
			totalfailedCount = totalfailedCount + failedList.get(i);
		}
		for (int i = 0; i < skippedList.size(); i++) {
			totalskippedCount = totalskippedCount + skippedList.get(i);
		}
		grandTotalCount = totalpassedCount + totalfailedCount + totalskippedCount;

		chartName = ChartGenerator.getChart(totalpassedCount, totalfailedCount, totalskippedCount);

		endTime = Utils.getCurrentTimeStamp();
		endTimeList.add(endTime);

		log.info("Execution Completed at: " + endTime);

		String timeElapsed = Utils.calculateTimeDifference(startTime, endTime);
		totalTimeList.add(timeElapsed);

		log.info("Time Taken for Execution: " + timeElapsed);

		String totalTimeTaken = Utils.calculateTotalTimeTaken(startTimeList, endTimeList);
		log.info("Total Time Taken for Execution: " + totalTimeTaken);

		String reportPath = "";
		reportPath = "<img src=\"" + chartName + "\">";

		ReportGenerator.writeToHTML(browserName, url, modulesList, TCList, totalList, passedList, failedList,
				skippedList, totalpassedCount, totalfailedCount, totalskippedCount, grandTotalCount, totalTimeList,
				reportPath, exceptionList, snapShotList, totalTimeTaken, Utils.getMessageBasedonTime(), tcDetails);

		String destpath = "";
		destpath = CURRENTDIR + fs + "TestAutomationReports" + fs + Utils.getCurrentTimeStamp();

		String testReportPath = System.getProperty("user.dir") + fs + "TestReport.html";

		File f = new File(testReportPath);
		if (!f.exists()) {
			f.createNewFile();
		}

//		FileUtils.copyDirectory(new File(System.getProperty("user.dir") + fs + "Snapshots" + fs), new File(destpath));
//		log.info("Folder: " + System.getProperty("user.dir") + fs + "Reports Copied to " + destpath);
		FileUtils.copyFileToDirectory(f, new File(destpath));

		log.info("File: " + CURRENTDIR + fs + "TestReport.html" + fs + " Copied to " + destpath);
		try {
			FileUtils.copyFileToDirectory(new File(System.getProperty("user.dir") + fs + "testng-failed.xml" + fs),
					new File(destpath));

			log.info("File: " + System.getProperty("user.dir") + fs + "testng-failed.xml Copied to " + destpath);
		} catch (FileNotFoundException fnf) {
			System.out.println(System.getProperty("user.dir") + fs + "testng-failed.xml does not exist");
		}

		extent.addSystemInfo("Time taken for execution", timeElapsed);

		// flush writes everything to the log file
		extent.flush();

		// Ending the test
		extent.endTest(test);

		// closing the extent report
		extent.close();
	}

	public String getModuleName() throws CloudLeafException {
		String className = this.getClass().getName();
		String myText = className.replace(".", "%");
		String s[] = myText.split("%");
		MODULENAME = s[s.length - 1];
		System.out.println("ModuleName: " + MODULENAME);
		return MODULENAME;
	}

	public String getTestCaseNameFromScript(String scriptName) {
		StringBuffer sb = new StringBuffer();
		String parts[] = scriptName.split("_");
		for (int i = 0; i < parts.length; i++) {
			if (i > 1) {
				sb.append(parts[i] + " ");
			}
		}
		return sb.toString().toUpperCase();
	}

	public static void main(String args[]) {
		System.out.println(new File(System.getProperty("user.dir") + Utils.getFileSeparator() + "TestReport.html"));
	}
}
