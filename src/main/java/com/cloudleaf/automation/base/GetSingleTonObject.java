package com.cloudleaf.automation.base;

import com.cloudleaf.automation.pages.CloudLeafLoginPage;
import com.cloudleaf.automation.pages.CommonPage;
import com.cloudleaf.automation.pages.DigitalTwinHomePage;
import com.cloudleaf.automation.pages.AssetDetailsPage;
import com.cloudleaf.automation.pages.AssetsPage;
import com.cloudleaf.automation.pages.ManageDigitalTwinPage;
import com.cloudleaf.automation.pages.MappingAdminPage;
import com.cloudleaf.automation.pages.SensorsPage;
import com.cloudleaf.automation.utils.PageNavigations;
import com.cloudleaf.automation.utils.RestAPIUtility;

public class GetSingleTonObject {

	public static CloudLeafLoginPage cloudLeafLoginPage = null;
	public static DigitalTwinHomePage digitalTwinHomePage = null;
	public static ManageDigitalTwinPage manageDigitalTwinPage = null;
	public static AssetsPage manageAssetsPage = null;
	public static SensorsPage sensorsPage = null;
	public static PageNavigations pageNavigations = null;
	public static CommonPage commonPage = null;
	public static MappingAdminPage mappingAdminPage = null;
	public static AssetDetailsPage assetDetailsPage = null;
	public static RestAPIUtility restAPIUtility = null;

	public static CloudLeafLoginPage getLoginPageObject() {
		if (cloudLeafLoginPage == null) {
			cloudLeafLoginPage = new CloudLeafLoginPage();
		}
		return cloudLeafLoginPage;
	}

	public static DigitalTwinHomePage getDigitalTwinHomePageObject() {
		if (digitalTwinHomePage == null) {
			digitalTwinHomePage = new DigitalTwinHomePage();
		}
		return digitalTwinHomePage;
	}

	public static ManageDigitalTwinPage getManageDigitalTwinPageObject() {
		if (manageDigitalTwinPage == null) {
			manageDigitalTwinPage = new ManageDigitalTwinPage();
		}
		return manageDigitalTwinPage;
	}


	public static AssetsPage getManageAssetsPageObject() {
		if (manageAssetsPage == null) {
			manageAssetsPage = new AssetsPage();
		}
		return manageAssetsPage;
	}
	
	
	public static SensorsPage getSensorsPageObject() {
		if (sensorsPage == null) {
			sensorsPage = new SensorsPage();
		}
		return sensorsPage;
	}
	
	public static PageNavigations getPageNavigationsObject() {
		if (pageNavigations == null) {
			pageNavigations = new PageNavigations();
		}
		return pageNavigations;
	}
	
	public static CommonPage getCommonPageObject() {
		if (commonPage == null) {
			commonPage = new CommonPage();
		}
		return commonPage;
	}
	
	public static MappingAdminPage getMappingAdminPageObject() {
		if (mappingAdminPage == null) {
			mappingAdminPage = new MappingAdminPage();
		}
		return mappingAdminPage;
	}
	
	public static AssetDetailsPage getAssetDetailsPageObject() {
		if (assetDetailsPage == null) {
			assetDetailsPage = new AssetDetailsPage();
		}
		return assetDetailsPage;
	}
	
	public static RestAPIUtility getRestAPIUtilityObject() {
		if (restAPIUtility == null) {
			restAPIUtility = new RestAPIUtility();
		}
		return restAPIUtility;
	}

}
