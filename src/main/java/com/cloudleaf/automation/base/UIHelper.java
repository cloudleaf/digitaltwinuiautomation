package com.cloudleaf.automation.base;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cloudleaf.automation.exceptions.CloudLeafException;
import com.cloudleaf.automation.utils.PropertyFileUtils;
import com.cloudleaf.automation.utils.Utils;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.relevantcodes.extentreports.LogStatus;

import org.apache.poi.ss.usermodel.Row;

public class UIHelper extends BasePage {
	static By obj;

	// DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH-mm-ss");
	// Date date = new Date();
	public String currentTime = new SimpleDateFormat("MM-dd-yyyy HH-mm-ss").format(new Date());

	public static void waitForElement(WebDriver driver, String xpath) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 100);

			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		} catch (Exception e) {

			System.out.println(xpath + "not found in the page");
			e.getStackTrace();
		}
		System.out.println(xpath + "element found");
		/*
		 * //click the element driver.findElement(By.xpath(xpath)).click();
		 * System.out.println(xpath + "element clicked");
		 */
	}

	public static String captureScreenShot(String fileName) throws IOException {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String filePath = Utils.SCREENSHOTS_DIR + fileName + "----"
				+ new SimpleDateFormat("MMMddyyyyHH-mm-ss").format(new Date()) + ".png";

		FileUtils.copyFile(source, new File(filePath));
		System.out.println("Screen Shot taken " + filePath);
		return filePath;
	}

	public static String captureScreeshot(String fileName) throws IOException {
		// String dest = "ScreenShots/"+fileName+"----"+new SimpleDateFormat("MMM dd
		// yyyy HH-mm-ss").format(new Date())+".png";
		String dest = "C:\\Users\\rallamsetti\\workspace\\com.cloudleaf.webautomation\\ScreenShots\\" + fileName
				+ ".png";
		// String dest = "\\ScreenShots\\"+fileName+".png";
		File destination = new File(dest);
		File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(source, destination);

		// dest = "/ScreenShots/"+filename+".jpg";
		return dest;

	}

	public static String captureScreeshot2() throws IOException {
		// String dest = "ScreenShots/"+fileName+"----"+new SimpleDateFormat("MMM dd
		// yyyy HH-mm-ss").format(new Date())+".png";
		// String dest =
		// "C:\\Users\\rallamsetti\\workspace\\com.cloudleaf.webautomation\\ScreenShots\\"+fileName+".png";
		String dest = System.getProperty("user.dir") + "\\ScreenShots\\"
				+ new SimpleDateFormat("MMM dd yyyy HH-mm-ss").format(new Date()) + ".png";
		File destination = new File(dest);
		File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(source, destination);

		// dest = "/ScreenShots/"+filename+".jpg";
		return dest;

	}

	public static String captureScreeshotMacPC() throws IOException {
		// String dest = "ScreenShots/"+fileName+"----"+new SimpleDateFormat("MMM dd
		// yyyy HH-mm-ss").format(new Date())+".png";
		// String dest =
		// "C:\\Users\\rallamsetti\\workspace\\com.cloudleaf.webautomation\\ScreenShots\\"+fileName+".png";
		String dest = System.getProperty("user.dir") + "/ScreenShots/"
				+ new SimpleDateFormat("MMMddyyyy-HH-mm-ss").format(new Date()) + ".png";
		File destination = new File(dest);
		File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(source, destination);

		// dest = "/ScreenShots/"+filename+".jpg";
		return dest;

	}

	public static void isElementExists(WebElement element) {
		if (element.isDisplayed()) {
			System.out.println(element + " is Displayed in the page");
		} else {
			System.out.println(element + " not Displayed in the page");
		}
	}

	public static boolean isElementNotExists(WebElement element) {
		boolean flag = false;
		try {
			element.isDisplayed();

		} catch (NoSuchElementException nse) {
			flag = true;
		}
		return flag;
	}

	public static void clickEnterKey() {
		new Actions(driver).sendKeys(Keys.ENTER).build().perform();
	}

	public static void pressTabKey() {
		new Actions(driver).sendKeys(Keys.TAB).build().perform();
	}

	public static WebDriver openBrowser(String browserName, String url, String os) {
		ChromeOptions chromeOptions = new ChromeOptions();
		HashMap<String, Object> chromePref = new HashMap<String, Object>();
		chromePref.put("download.default_directory", System.getProperty("java.io.tmpdir"));
		chromeOptions.setExperimentalOption("prefs", chromePref);
		System.out.println("Opening the browser");

		if (browserName.equalsIgnoreCase("firefox")) {
			if (os.equalsIgnoreCase("windows")) {
				/*
				 * FirefoxProfile firefoxProfile = new FirefoxProfile();
				 * firefoxProfile.setPreference("browser.private.browsing.autostart",true);
				 * driver = new FirefoxDriver(firefoxProfile);
				 */

				System.setProperty("webdriver.gecko.driver", "./Drivers/windows/geckodriver.exe");
				// driver.get(url);
			} else if (os.equalsIgnoreCase("linux")) {
				System.setProperty("webdriver.gecko.driver", "./Drivers/linux/geckodriver");

			}

			else if (os.equalsIgnoreCase("mac")) {
				System.setProperty("webdriver.gecko.driver", "./Drivers/mac/geckodriver");

			}
			driver = new FirefoxDriver();
		}

		else if (browserName.equalsIgnoreCase("chrome")) {
			if (os.equalsIgnoreCase("windows"))

			{
				// To Open the chrome browser in incongnit window
				/*
				 * DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				 * ChromeOptions options = new ChromeOptions();
				 * options.addArguments("--incognito");
				 * capabilities.setCapability(ChromeOptions.CAPABILITY, options); driver = new
				 * ChromeDriver(capabilities);
				 */

//				System.setProperty("webdriver.chrome.driver", "Drivers/windows/chromedriver.exe");
				
				try
				{
				chromeOptions.setBinary("C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe");
				}
				catch(WebDriverException wde)
				{
					chromeOptions.setBinary("C:\\PROGRA~2\\Google\\Chrome\\Application\\chrome.exe");
				}
				File file = new File(PropertyFileUtils.DRIVERS_FOLDER + "chromedriver.exe");
				System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
				/*
				 * System.out.println("Opening the chrome browser in incongnito mode ");
				 * 
				 * driver.get(url);
				 */

			}

			else if (os.equalsIgnoreCase("linux")) {
				System.setProperty("webdriver.chrome.driver", "Drivers/linux/chromedriver");

			} else if (os.equalsIgnoreCase("mac")) {
				System.out.println("");
				System.setProperty("webdriver.chrome.driver", "Drivers/mac/chromedriver");

			}

			driver = new ChromeDriver(chromeOptions);
		}

		else if (browserName.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", "./Drivers/IEDriverServer.exe");
			driver = new InternetExplorerDriver();

		}

		else if (browserName.equalsIgnoreCase("edge")) {
			System.setProperty("webdriver.edge.driver", "./Drivers/windows/MicrosoftWebDriver.exe");
			driver = new EdgeDriver();
		}

		else if (browserName.equalsIgnoreCase("safari")) {
			driver = new SafariDriver();

		}

		driver.manage().window().maximize();

		driver.manage().deleteAllCookies();

		driver.get(url);

		return driver;
	}

	public static void softAssertValidateText(String actual, String expected) {
		SoftAssert sassert = new SoftAssert();
		sassert.assertEquals(actual, expected);

		// System.out.println("Assertion Passed");

	}

	public static void softAssertEquals(String actual, String expected, String message) {
		SoftAssert sassert = new SoftAssert();
		sassert.assertEquals(actual, expected, message);
		// System.out.println("Assertion Passed");

	}

	public static void softAssertTrue(boolean condition, String message) {
		SoftAssert sassert = new SoftAssert();
		sassert.assertTrue(condition, message);
		// System.out.println("Assertion Passed");

	}

	public static void softAssertAll() {
		SoftAssert sassert = new SoftAssert();
		sassert.assertAll();
		// System.out.println("Assertion Passed");

	}

	public static void selectDropDownValue(WebElement element, String value) throws InterruptedException {
		Select sel = new Select(element);
		sel.selectByValue(value);
	}

	public static void ValidateText(String actual, String expected) {

		Assert.assertEquals(actual, expected);
		System.out.println("Assertion Passed");

	}

	public static ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
		@Override
		public Boolean apply(WebDriver driver) {
			return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
					.equals("complete");
		}
	};

	public static ExpectedCondition<Boolean> xhrLoad = new ExpectedCondition<Boolean>() {
		@Override
		public Boolean apply(WebDriver driver) {
			return ((JavascriptExecutor) driver).executeScript("return return xhr.readyState;").toString().equals("4");
		}
	};

	public static ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
		@Override
		public Boolean apply(WebDriver driver) {
			try {
				return (Boolean) ((JavascriptExecutor) driver)
						.executeScript("return window.jQuery != undefined || jQuery.active == 0");
			} catch (Exception e) {
				return true;
			}
		}
	};

	public static boolean waitForPageLoaded() {
		boolean flag = true;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			flag = wait.until(xhrLoad) && wait.until(jQueryLoad) && wait.until(jsLoad);
			return flag;
		} catch (Exception e) {
			return false;
		}
	}

	public static void waitUntilElement(WebElement element) throws CloudLeafException {
		try {
			Thread.sleep(1000);
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new CloudLeafException(e.getMessage());
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			throw new CloudLeafException(e.getMessage());
		}

	}

	public static void waitUntilElementClickable(WebElement element) {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		WebDriverWait wait = new WebDriverWait(driver, 70);
		WebDriverWait wait = new WebDriverWait(driver, 15);
		// wait.until(ExpectedConditions.presenceOfElementLocated(obj));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		// wait.until(ExpectedConditions.textToBePresentInElement(element, text))

	}

	public static void waitUntilTextPresent(WebElement element, String text) {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebDriverWait wait = new WebDriverWait(driver, 100);
		// wait.until(ExpectedConditions.presenceOfElementLocated(obj));
		wait.until(ExpectedConditions.textToBePresentInElement(element, text));

	}

	public static void waitUntilElement(String locator, String locatorValue) {
		By obj = byObjectReturn(locator, locatorValue);
		WebDriverWait wait = new WebDriverWait(driver, 60);
		// wait.until(ExpectedConditions.presenceOfElementLocated(obj));
		wait.until(ExpectedConditions.visibilityOfElementLocated(obj));

	}

	public static By byObjectReturn(String locator, String locatorvalue) {

		if (locator.equals("id")) {
			obj = By.id(locatorvalue);
		} else if (locator.equals("name")) {
			obj = By.name(locatorvalue);
		} else if (locator.equals("xpath")) {
			obj = By.xpath(locatorvalue);
		} else if (locator.equals("linkText")) {
			obj = By.linkText(locatorvalue);
		}

		else if (locator.equals("className")) {
			obj = By.className(locatorvalue);
		} else if (locator.equals("partialLinkText")) {
			obj = By.partialLinkText(locatorvalue);
		} else if (locator.equals("tagName")) {
			obj = By.tagName(locatorvalue);
		}

		return obj;
	}

	public static void fileUpload(String pathAndFileName) throws Exception {

		StringSelection stringSelection = new StringSelection(pathAndFileName);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.setAutoDelay(3000);

		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.setAutoDelay(3000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.setAutoDelay(3000);
		System.out.println("File uploaded successfully!!!");
	}

	/**
	 * Js click.
	 *
	 * @param myWebElement the my web element
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws CloudLeafException 
	 */
	public static void jsClick(WebElement element) throws IOException, InterruptedException, CloudLeafException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		waitUntilElement(element);
		highlightElement(element);
		String snapshotName = UIHelper.captureScreenShot("DT");
		test.log(LogStatus.INFO, test.addScreenCapture(snapshotName));
		Thread.sleep(500);
		try {

			((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			throw new RuntimeException("Failed: to click the webelement " + e.getMessage());
		}
		removeHighlightElement(element);
	}

	/**
	 * Js refresh browser.
	 */
	public void jsRefreshBrowser() {
		try {
			((JavascriptExecutor) driver).executeScript("history.go(0)");
		} catch (Exception e) {
			throw new RuntimeException("Failed: to Refresh the browser " + e.getMessage());
		}
	}

	/**
	 * Js inner text.
	 *
	 * @return the string
	 */
	public String jsInnerText() {
		try {
			String sText = ((JavascriptExecutor) driver).executeScript("return document.documentElement.innerText;")
					.toString();
			return sText;
		} catch (Exception e) {
			throw new RuntimeException("Failed: to get the inner text of the page" + e.getMessage());
		}
	}

	/**
	 * Js scroll window.
	 */
	public void jsScrollWindow() {
		try {
			// Vertical scroll - down by 150 pixels
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0,150)");
		} catch (Exception e) {
			throw new RuntimeException("Failed: to scroll the window " + e.getMessage());
		}
	}

	/**
	 * Js drag and drop.
	 *
	 * @param dragElementFrom the drag element from
	 * @param xTo             the x to
	 * @param yTo             the y to
	 */
	public void jsDragAndDrop(WebElement dragElementFrom, int xTo, int yTo) {
		try {
			// To drag and drop element by 100 pixel offset In horizontal
			// direction X
			((JavascriptExecutor) driver).executeScript(
					"function simulate(f,c,d,e){var b,a=null;for(b in eventMatchers)if(eventMatchers[b].test(c)){a=b;break}if(!a)return!1;document.createEvent?(b=document.createEvent(a),a==\"HTMLEvents\"?b.initEvent(c,!0,!0):b.initMouseEvent(c,!0,!0,document.defaultView,0,d,e,d,e,!1,!1,!1,!1,0,null),f.dispatchEvent(b)):(a=document.createEventObject(),a.detail=0,a.screenX=d,a.screenY=e,a.clientX=d,a.clientY=e,a.ctrlKey=!1,a.altKey=!1,a.shiftKey=!1,a.metaKey=!1,a.button=1,f.fireEvent(\"on\"+c,a));return!0} var eventMatchers={HTMLEvents:/^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,MouseEvents:/^(?:click|dblclick|mouse(?:down|up|over|move|out))$/}; "
							+ "simulate(arguments[0],\"mousedown\",0,0); simulate(arguments[0],\"mousemove\",arguments[1],arguments[2]); simulate(arguments[0],\"mouseup\",arguments[1],arguments[2]); ",
					dragElementFrom, xTo, yTo);
		} catch (Exception e) {
			throw new RuntimeException("Failed: to drag and drop the element " + e.getMessage());
		}
	}

	/**
	 * Js get title.
	 *
	 * @return the string
	 */
	public String jsGetTitle() {
		try {
			String sText = ((JavascriptExecutor) driver).executeScript("return document.title;").toString();
			return sText;
		} catch (Exception e) {
			throw new RuntimeException("Failed: to get the title of the web page " + e.getMessage());
		}
	}

	/**
	 * Js enable element.
	 *
	 * @param element the element
	 */
	public void jsEnableElement(WebElement element) {
		try {
			String js = "arguments[0].style.height='auto'; arguments[0].style.visibility='visible';";
			((JavascriptExecutor) driver).executeScript(js, element);
		} catch (Exception e) {
			throw new RuntimeException("Failed: to get the title of the web page " + e.getMessage());
		}
	}

	/**
	 * Js generate alert popup.
	 *
	 * @param alert_message the alert message
	 */
	public void jsGenerateAlertPopup(String alert_message) {
		try {
			if (alert_message.isEmpty()) {
				alert_message = "Test alert";
			}
			((JavascriptExecutor) driver).executeScript("alert(alert_message);");
		} catch (Exception e) {
			throw new RuntimeException("Failed: to generate the alert popup " + e.getMessage());
		}
	}

	public List<String> readCSVFile(String fileName) throws Exception {
		String line;
		List<String> lst = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		while ((line = br.readLine()) != null) {
			lst.add(line);
		}
		br.close();
		return lst;
	}

	public static void sendMail(String fileNameWithPath, int passCount, int failCount, String subjectMsg) {

		final String username = "ramesh531dummy@gmail.com";
		final String password = "Easwar123$";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ramesh531dummy@gmail.com"));

			String to = "rallamsetti@cloudleaf.io, sbondili@cloudleaf.io,ashaik@cloudleaf.io,pbabu@cloudleaf.io,pdunna@cloudleaf.io,rroopreddy@cloudleaf.io,mpathakota@cloudleaf.io";

			InternetAddress[] parse = InternetAddress.parse(to, true);
			message.setRecipients(Message.RecipientType.TO, parse);

			// message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("rallamsetti@cloudleaf.io"));
			// message.setRecipients(Message.RecipientType.CC,InternetAddress.parse("sbondili@cloudleaf.io"));

			message.setSubject(subjectMsg);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Now set the actual message

			messageBodyPart.setText("TOTAL TESTS PASSED:  " + passCount + "\n" + "TOTAL TESTS FAILED:  " + failCount
					+ "\n"
					+ "TEST RAIL REPORT URL : https://cloudleaf.testrail.io/index.php?/runs/view/197&group_by=tests:status_id&group_order=desc"
					+ "\n" + "Regards" + "\n" + "CloudLeaf Automation Team");
			// messageBodyPart.setText("TOTAL TESTS FAILED: "+failCount);

			// messageBodyPart.setText("Automation Reprot html "+fileNameWithPath);

			// Create a multipar message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			messageBodyPart = new MimeBodyPart();
			// Attach the xl file
			// String filename = "currencyValue.xls";
			DataSource source = new FileDataSource(fileNameWithPath);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(fileNameWithPath);
			multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts
			message.setContent(multipart);

			Transport.send(message);

			System.out.println("Email with Automation Report sent Successfully");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public static void sendMailwithTestRailReport(String fileNameWithPath, String testRailReport, int passCount,
			int failCount, String subjectMsg, int testRailRunID) {

		final String username = "ramesh531dummy@gmail.com";
		final String password = "Easwar123$";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		String msgBody = "TOTAL TESTS PASSED:  " + passCount + "\n" + "TOTAL TESTS FAILED:  " + failCount + "\n"
				+ "TEST RAIL REPORT URL : https://cloudleaf.testrail.io/index.php?/runs/view/" + testRailRunID + "\n"
				+ "Regards" + "\n" + "CloudLeaf Automation Team";

		// DataSource source;

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ramesh531dummy@gmail.com"));

			String to = "rallamsetti@cloudleaf.io, sbondili@cloudleaf.io,ashaik@cloudleaf.io,pbabu@cloudleaf.io,pdunna@cloudleaf.io,rroopreddy@cloudleaf.io,mpathakota@cloudleaf.io";

			InternetAddress[] parse = InternetAddress.parse(to, true);
			message.setRecipients(Message.RecipientType.TO, parse);

			message.setSubject(subjectMsg);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Now set the actual message

			messageBodyPart.setText(msgBody);
			// Create a multipart message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			// messageBodyPart = new MimeBodyPart();

			// Attach the report html file
			// DataSource source = new FileDataSource(fileNameWithPath);
			// messageBodyPart.setDataHandler(new DataHandler(source));
			// messageBodyPart.setFileName(fileNameWithPath);
			// multipart.addBodyPart(messageBodyPart);
			//
			//
			// DataSource source2 = new FileDataSource(testRailReport);
			// messageBodyPart.setDataHandler(new DataHandler(source2));
			// messageBodyPart.setFileName(testRailReport);
			// multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts
			// message.setContent(multipart);

			ArrayList<String> al = new ArrayList<String>();
			al.add(fileNameWithPath);
			al.add(testRailReport);
			for (int i = 0; i < al.size(); i++) {
				System.out.println(al.get(i));

				messageBodyPart = new MimeBodyPart();
				DataSource source = new FileDataSource((String) al.get(i));

				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName((String) al.get(i));
				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);
			}

			Transport.send(message);

			System.out.println("Email with Automation Report and TESTRAIL Report sent Successfully");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public static void sendMailwithTestRailReport_QA(String fileNameWithPath, String testRailReport, int passCount,
			int failCount, String subjectMsg, int testRailRunID) {

		final String username = "ramesh531dummy@gmail.com";
		final String password = "Easwar123$";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		String msgBody = "TOTAL TESTS PASSED:  " + passCount + "\n" + "TOTAL TESTS FAILED:  " + failCount + "\n"
				+ "TEST RAIL REPORT URL : https://cloudleaf.testrail.io/index.php?/runs/view/" + testRailRunID + "\n"
				+ "Regards" + "\n" + "CloudLeaf Automation Team";

		// DataSource source;

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ramesh531dummy@gmail.com"));

			String to = "rallamsetti@cloudleaf.io, sbondili@cloudleaf.io,ashaik@cloudleaf.io,steegala@cloudleaf.io,schidurula@cloudleaf.io";

			InternetAddress[] parse = InternetAddress.parse(to, true);
			message.setRecipients(Message.RecipientType.TO, parse);

			message.setSubject(subjectMsg);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Now set the actual message

			messageBodyPart.setText(msgBody);
			// Create a multipart message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			// messageBodyPart = new MimeBodyPart();

			// Attach the report html file
			// DataSource source = new FileDataSource(fileNameWithPath);
			// messageBodyPart.setDataHandler(new DataHandler(source));
			// messageBodyPart.setFileName(fileNameWithPath);
			// multipart.addBodyPart(messageBodyPart);
			//
			//
			// DataSource source2 = new FileDataSource(testRailReport);
			// messageBodyPart.setDataHandler(new DataHandler(source2));
			// messageBodyPart.setFileName(testRailReport);
			// multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts
			// message.setContent(multipart);

			ArrayList<String> al = new ArrayList<String>();
			al.add(fileNameWithPath);
			al.add(testRailReport);
			for (int i = 0; i < al.size(); i++) {
				System.out.println(al.get(i));

				messageBodyPart = new MimeBodyPart();
				DataSource source = new FileDataSource((String) al.get(i));

				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName((String) al.get(i));
				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);
			}

			Transport.send(message);

			System.out.println("Email with Automation Report and TESTRAIL Report sent Successfully");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public static void sendMailwithTestRailURL(String fileNameWithPath, int passCount, int failCount,
			String subjectMsg) {

		final String username = "ramesh531dummy@gmail.com";
		final String password = "Easwar123$";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		// DataSource source;

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ramesh531dummy@gmail.com"));

			String to = "rallamsetti@cloudleaf.io, sridhar.bathina@cloudleaf.io,pbabu@cloudleaf.io,pdunna@cloudleaf.io,rroopreddy@cloudleaf.io,mpathakota@cloudleaf.io";
			InternetAddress[] parse = InternetAddress.parse(to, true);
			message.setRecipients(Message.RecipientType.TO, parse);

			message.setSubject(subjectMsg);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Now set the actual message

			messageBodyPart.setText("TOTAL TESTS PASSED:  " + passCount + "\n" + "TOTAL TESTS FAILED:  " + failCount
					+ "\n"
					+ "TEST RAIL REPORT URL : https://cloudleaf.testrail.io/index.php?/runs/view/197&group_by=tests:status_id&group_order=desc"
					+ "\n" + "Regards" + "\n" + "CloudLeaf Automation Team");

			// Create a multipart message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			messageBodyPart = new MimeBodyPart();

			// Attach the report html file
			DataSource source = new FileDataSource(fileNameWithPath);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(fileNameWithPath);
			multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts
			message.setContent(multipart);

			Transport.send(message);

			System.out.println("Email with Automation Report and TESTRAIL Report sent Successfully");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	// private static void addAttachment(Multipart multipart, String filename)
	// throws MessagingException
	// {
	// DataSource source = new FileDataSource(filename);
	// BodyPart messageBodyPart = new MimeBodyPart();
	// messageBodyPart.setDataHandler(new DataHandler(source));
	// messageBodyPart.setFileName(filename);
	// multipart.addBodyPart(messageBodyPart);
	// }

	public static void sendMailCI(String fileNameWithPath, int passCount, int failCount, String subjectMsg) {

		final String username = "ramesh531dummy@gmail.com";
		final String password = "Easwar123$";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ramesh531dummy@gmail.com"));

			String to = "rallamsetti@cloudleaf.io, sbondili@cloudleaf.io, srinathreddy@cloudleaf.io,kmulugu@cloudleaf.io";
			InternetAddress[] parse = InternetAddress.parse(to, true);
			message.setRecipients(Message.RecipientType.TO, parse);

			// message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("rallamsetti@cloudleaf.io"));
			// message.setRecipients(Message.RecipientType.CC,InternetAddress.parse("sbondili@cloudleaf.io"));

			message.setSubject(subjectMsg);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Now set the actual message

			messageBodyPart.setText("TOTAL TESTS PASSED:  " + passCount + "\n" + "TOTAL TESTS FAILED:  " + failCount
					+ "\n" + "Regards" + "\n" + "CloudLeaf Automation Team");
			// messageBodyPart.setText("TOTAL TESTS FAILED: "+failCount);

			// messageBodyPart.setText("Automation Reprot html "+fileNameWithPath);

			// Create a multipar message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Part two is attachment
			messageBodyPart = new MimeBodyPart();
			// Attach the xl file
			// String filename = "currencyValue.xls";
			DataSource source = new FileDataSource(fileNameWithPath);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(fileNameWithPath);
			multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts
			message.setContent(multipart);

			Transport.send(message);

			System.out.println("Email with Automation Report sent Successfully");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws Exception {
		String result = "";
		int partCount = mimeMultipart.getCount();
		for (int i = 0; i < partCount; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				// result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
				result = html;
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}

	public static String confirmYourAccountEmail() {
		String bodyContent = "";
		try {

			// create properties field
			Properties properties = new Properties();

			// Connect through imap
			properties.put("mail.imap.host", "pop.gmail.com");
			properties.put("mail.imap.port", "993");
			properties.put("mail.imap.starttls.enable", "true");
			properties.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			properties.setProperty("mail.imap.socketFactory.fallback", "false");
			properties.setProperty("mail.imap.socketFactory.port", String.valueOf(993));
			Session emailSession = Session.getDefaultInstance(properties);

			// create the POP3 store object and connect with the pop server
			Store store = emailSession.getStore("imap");

			store.connect("pop.gmail.com", "ramesh531dummy@gmail.com", "Easwar123$");

			// create the folder object and open it
			Folder emailFolder = store.getFolder("Inbox");
			emailFolder.open(Folder.READ_ONLY);

			// retrieve the messages from the folder in an array and print it
			Message[] messages = emailFolder.getMessages();
			/*
			 * System.out.println("Total Mails "+emailFolder.getMessageCount());
			 * System.out.println("Total UNREAD Mails "+emailFolder.getUnreadMessageCount())
			 * ; System.out.println("Has New messages :"+ emailFolder.hasNewMessages());
			 * System.out.println("messages.length---" + messages.length);
			 */

			// Reading the latest email

			for (int i = messages.length - 1; i >= messages.length - 1; i--) {
				Message message = messages[i];
				System.out.println("---------------------------------");
				System.out.println("Email Number " + (i + 1));
				System.out.println("Subject: " + message.getSubject());
				System.out.println("Received Date : " + message.getReceivedDate());

				System.out.println("From: " + message.getFrom()[0]);
				System.out.println("message type Plain : " + message.isMimeType("text/plain"));
				System.out.println("message type Mulitpart : " + message.isMimeType("multipart/*"));

				MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
				bodyContent = getTextFromMimeMultipart(mimeMultipart);
				System.out.println("Body Content for Confirm Your Account Email " + bodyContent);

				// System.out.println("Confirm Email link : " + bodyContent.substring(206,292));
				// confirmEmailLink = bodyContent.substring(222,308);
				// System.out.println("Confirm Email link :" +confirmEmailLink);

			}
			// close the store and folder objects
			emailFolder.close(false);
			store.close();

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bodyContent;
	}

	public static String resetYourAccountEmail() {
		String bodyContent = "";
		try {

			// create properties field
			Properties properties = new Properties();

			// Connect through imap
			properties.put("mail.imap.host", "pop.gmail.com");
			properties.put("mail.imap.port", "993");
			properties.put("mail.imap.starttls.enable", "true");
			properties.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			properties.setProperty("mail.imap.socketFactory.fallback", "false");
			properties.setProperty("mail.imap.socketFactory.port", String.valueOf(993));
			Session emailSession = Session.getDefaultInstance(properties);

			// create the POP3 store object and connect with the pop server
			Store store = emailSession.getStore("imap");

			store.connect("pop.gmail.com", "ramesh531dummy@gmail.com", "Easwar123$");

			// create the folder object and open it
			Folder emailFolder = store.getFolder("Inbox");
			emailFolder.open(Folder.READ_ONLY);

			// retrieve the messages from the folder in an array and print it
			Message[] messages = emailFolder.getMessages();

			// Reading the latest email

			for (int i = messages.length - 1; i >= messages.length - 1; i--) {
				Message message = messages[i];
				System.out.println("---------------------------------");
				System.out.println("Email Number " + (i + 1));
				System.out.println("Subject: " + message.getSubject());
				System.out.println("Received Date : " + message.getReceivedDate());

				System.out.println("From: " + message.getFrom()[0]);
				System.out.println("message type Plain : " + message.isMimeType("text/plain"));
				System.out.println("message type Mulitpart : " + message.isMimeType("multipart/*"));

				MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
				bodyContent = getTextFromMimeMultipart(mimeMultipart);
				System.out.println("Body Content for Reset Your Account Email : " + bodyContent);

				// System.out.println("Confirm Email link : " + bodyContent.substring(206,292));
				// confirmEmailLink = bodyContent.substring(222,308);
				// System.out.println("Confirm Email link :" +confirmEmailLink);

			}
			// close the store and folder objects
			emailFolder.close(false);
			store.close();

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bodyContent;
	}

	public void clickTabKey() throws AWTException {
		Robot robot = new Robot();

		robot.keyPress(KeyEvent.VK_TAB);

		robot.keyRelease(KeyEvent.VK_TAB);
	}

	public static void writeDataToExcel(Map<String, Object[]> pageinfo, String filePath) throws Exception {

		// Create blank workbook
		XSSFWorkbook workbook = new XSSFWorkbook();

		// Create a blank sheet
		XSSFSheet spreadsheet = workbook.createSheet(" PageLoadTime ");

		// Create row object
		XSSFRow row;

		// Iterate over data and write to sheet
		Set<String> keyid = pageinfo.keySet();
		int rowid = 0;

		for (String key : keyid) {
			row = spreadsheet.createRow(rowid++);
			Object[] objectArr = pageinfo.get(key);
			int cellid = 0;

			for (Object obj : objectArr) {
				Cell cell = row.createCell(cellid++);
				cell.setCellValue((String) obj);
			}
		}
		// Write the workbook in file system
		FileOutputStream out = new FileOutputStream(new File(filePath));

		workbook.write(out);
		out.close();
		System.out.println("Data Written to Excel successfully");
	}

	public static ResultSet executeCassandraQueries(String query, String ipAddress) {

		// Creating Cluster object
		Cluster cluster = Cluster.builder().addContactPoint(ipAddress).build();

		// Creating Session object
		com.datastax.driver.core.Session session = cluster.connect("cloudleaf");

		// Getting the ResultSet
		ResultSet result = session.execute(query);

		return result;
	}

	// Added by Satish on 20 Feb, 2020
	public static Object[][] readTableData(String path, String sheetName) throws Exception {
		FileInputStream stream = new FileInputStream(path);
		Workbook workbook = WorkbookFactory.create(stream);
		Sheet s = workbook.getSheet(sheetName);

		int rowCount = s.getLastRowNum();
		// int rowCount = s.getLastRowNum()-s.getFirstRowNum();
		int colCount = s.getRow(0).getLastCellNum();

		System.out.println("NUMBER OF ROWS : " + (rowCount + 1));
		System.out.println("NUMBER OF COLUMNS : " + colCount);

		Object data[][] = new Object[rowCount + 1][colCount];

		// for (int i = 0; i <=rowCount; i++) {
		// for (int j = 0; j < colCount; j++) {
		//
		// data[i][j] = s.getRow(i).getCell(j).getStringCellValue();
		// //System.out.print(data[i][j] + "\t");
		//
		// }
		// // System.out.println();
		// }

		for (int i = 0; i <= rowCount; i++) {

			org.apache.poi.ss.usermodel.Row r = s.getRow(i);

			for (int j = 0; j < colCount; j++) {
				Cell c = r.getCell(j);
				try {
					if (c.getCellType() == c.CELL_TYPE_STRING) {
						data[i][j] = c.getStringCellValue();
					} else {
						data[i][j] = String.valueOf(c.getNumericCellValue());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return data;
	}

	// Added by Satish on 20 Feb, 2020
	public static String[][] readTableDataAsString(String path, String sheetName) throws Exception {
		FileInputStream stream = new FileInputStream(path);
		Workbook workbook = WorkbookFactory.create(stream);
		Sheet s = workbook.getSheet(sheetName);

		int rowCount = s.getLastRowNum();
		// int rowCount = s.getLastRowNum()-s.getFirstRowNum();
		int colCount = s.getRow(0).getLastCellNum();

		System.out.println("NUMBER OF ROWS : " + (rowCount + 1));
		System.out.println("NUMBER OF COLUMNS : " + colCount);

		String data[][] = new String[rowCount + 1][colCount];
		// To ignore the first row you should start from i=1
		for (int i = 0; i <= rowCount; i++) {
			org.apache.poi.ss.usermodel.Row r = s.getRow(i);

			for (int j = 0; j < colCount; j++) {
				Cell c = r.getCell(j);
				try {
					if (c.getCellType() == c.CELL_TYPE_STRING) {
						data[i][j] = c.getStringCellValue();
					} else {
						data[i][j] = String.valueOf(c.getNumericCellValue());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return data;
	}

	public static String[][] readExcelDataWithoutHeaders(String path, String sheetName) throws Exception {
		FileInputStream stream = new FileInputStream(path);
		Workbook workbook = WorkbookFactory.create(stream);
		Sheet s = workbook.getSheet(sheetName);

		int rowCount = s.getLastRowNum();
		// int rowCount = s.getLastRowNum()-s.getFirstRowNum();
		int colCount = s.getRow(0).getLastCellNum();

		System.out.println("NUMBER OF ROWS : " + (rowCount + 1));
		System.out.println("NUMBER OF COLUMNS : " + colCount);

		String data[][] = new String[rowCount + 1][colCount];
		// To ignore the first row you should start from i=1
		for (int i = 1; i <= rowCount; i++) {
			org.apache.poi.ss.usermodel.Row r = s.getRow(i);

			for (int j = 0; j < colCount; j++) {
				Cell c = r.getCell(j);
				try {
					if (c.getCellType() == c.CELL_TYPE_STRING) {
						data[i][j] = c.getStringCellValue();
						System.out.println(data[i][j]);
					} else {
						data[i][j] = String.valueOf(c.getNumericCellValue());
						System.out.println(data[i][j]);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return data;
	}

	public static String[][] readExcel(String filePath, String fileName, String sheetName) throws IOException {

		File file = new File(filePath + "/" + fileName);
		FileInputStream inputStream = new FileInputStream(file);
		Workbook myWorkbook = null;
		String fileExtensionName = fileName.substring(fileName.indexOf("."));

		if (fileExtensionName.equals(".xlsx")) {

			myWorkbook = new XSSFWorkbook(inputStream);

		}

		else if (fileExtensionName.equals(".xls")) {

			myWorkbook = new HSSFWorkbook(inputStream);

		}

		// Read sheet inside the workbook by its name

		Sheet guru99Sheet = myWorkbook.getSheet(sheetName);
		Row row;

		// Find number of rows in excel file

		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();
		int cellCount = guru99Sheet.getRow(0).getLastCellNum();

		System.out.println("NUMBER OF ROWS : " + rowCount);
		System.out.println("NUMBER OF COLUMNS : " + cellCount);

		String data[][] = new String[rowCount][cellCount];

		// Create a loop over all the rows of excel file to read it . To ignore the
		// first row Headers you should loop through from 1

		for (int i = 1; i < rowCount + 1; i++) {

			row = guru99Sheet.getRow(i);

			// Create a loop to print cell values in a row

			for (int j = 0; j < row.getLastCellNum(); j++) {

				// Print Excel data in console
				System.out.println("data[" + i + "][" + j + "]");
				data[i][j] = row.getCell(j).getStringCellValue();

				System.out.print(row.getCell(j).getStringCellValue() + "|| ");

			}

			System.out.println();
		}
		return data;

	}

	public void readExcel1(String filePath, String fileName, String sheetName) throws IOException {

		// Create an object of File class to open xlsx file

		File file = new File(filePath + "/" + fileName);

		// Create an object of FileInputStream class to read excel file

		FileInputStream inputStream = new FileInputStream(file);

		Workbook myWorkbook = null;

		String fileExtensionName = fileName.substring(fileName.indexOf("."));

		if (fileExtensionName.equals(".xlsx")) {

			myWorkbook = new XSSFWorkbook(inputStream);

		} else if (fileExtensionName.equals(".xls")) {
			myWorkbook = new HSSFWorkbook(inputStream);

		}

		Sheet guru99Sheet = myWorkbook.getSheet(sheetName);

		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();

		// Create a loop over all the rows of excel file to read it . To ignore the
		// first row Headers you should loop through from 1

		for (int i = 1; i < rowCount + 1; i++) {

			Row row = guru99Sheet.getRow(i);

			// Create a loop to print cell values in a row

			for (int j = 0; j < row.getLastCellNum(); j++) {

				System.out.print(row.getCell(j).getStringCellValue() + "|| ");

			}

			System.out.println();
		}

	}

	public static void hoverOnElement(WebElement elem) {
		Actions actions = new Actions(driver);
		actions.moveToElement(elem).build().perform();
	}

	public String digitStripCount(List<WebElement> digitStripPath)
			throws HeadlessException, UnsupportedFlavorException, IOException {
		String result = "";
		List<String> visibleCount = new ArrayList<String>();
		for (int i = 0; i < digitStripPath.size(); i++) {
			Actions actions = new Actions(driver);
			actions.doubleClick(digitStripPath.get(i)).perform();
			actions.keyDown(Keys.CONTROL).sendKeys("c").keyUp(Keys.CONTROL).perform();
			String VisibleNumber = (String) Toolkit.getDefaultToolkit().getSystemClipboard()
					.getData(DataFlavor.stringFlavor);
			visibleCount.add(VisibleNumber);
			result = (new StringBuilder()).append(visibleCount.get(i)).append(result).toString();
		}
		return result;
	}

	public static void closeBrowser() {
		driver.close();
		driver.quit();
	}

	public static void highlightElement(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
	}

	public static void removeHighlightElement(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].removeAttribute('style', '');", element);
	}

	public static void enterText(WebElement element, String input) throws IOException, InterruptedException, CloudLeafException {

		waitUntilElement(element);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		highlightElement(element);
		try {
			element.clear();
			Thread.sleep(500);
			element.sendKeys(input);
		} catch (Exception e) {
			System.out.println("Element is not interactable");
		}
		String snapshotName = UIHelper.captureScreenShot("DT");
		test.log(LogStatus.INFO, test.addScreenCapture(snapshotName));
		removeHighlightElement(element);
	}

	public static void enterCharByChar(WebElement element, String input) throws IOException, InterruptedException, CloudLeafException {

		waitUntilElement(element);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		highlightElement(element);
		try {
			element.clear();
			Thread.sleep(500);
			for (int i = 0; i < input.length(); i++) {
				char c = input.charAt(i);
				String s = new StringBuilder().append(c).toString();
				element.sendKeys(s);
				Thread.sleep(250);
			}
		} catch (Exception e) {
			System.out.println("Element is not interactable");
		}
		String snapshotName = UIHelper.captureScreenShot("DT");
		test.log(LogStatus.INFO, test.addScreenCapture(snapshotName));
		removeHighlightElement(element);
	}

	public static void pressBackspaceKey(WebElement element) throws CloudLeafException {
		waitUntilElement(element);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		highlightElement(element);
		while (element.getAttribute("value").length() > 0) {
			element.sendKeys(Keys.BACK_SPACE);
		}
		removeHighlightElement(element);
	}

	public static void clickElement(WebElement element) throws IOException, InterruptedException, CloudLeafException {

		waitUntilElementClickable(element);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		highlightElement(element);
		String snapshotName = UIHelper.captureScreenShot("DT");
		test.log(LogStatus.INFO, test.addScreenCapture(snapshotName));
		Thread.sleep(500);
		try {
			element.click();
		} catch (Exception e) {
			jsClick(element);
		}
		try {
			removeHighlightElement(element);
		} catch (Exception e) {
			System.out.println("Element not found in page !");
		}
	}

	public static boolean isElementDisplayed(WebElement element) throws IOException, InterruptedException, CloudLeafException {
		waitUntilElement(element);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		waitUntilElement(element);
		highlightElement(element);
		String snapshotName = UIHelper.captureScreenShot("DT");
		test.log(LogStatus.INFO, test.addScreenCapture(snapshotName));
		Thread.sleep(500);
		removeHighlightElement(element);
		return element.isDisplayed();
	}

	public static boolean isElementEnabled(WebElement element) throws IOException, InterruptedException, CloudLeafException {
		waitUntilElement(element);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		waitUntilElement(element);
		highlightElement(element);
		String snapshotName = UIHelper.captureScreenShot("DT");
		test.log(LogStatus.INFO, test.addScreenCapture(snapshotName));
		Thread.sleep(500);
		removeHighlightElement(element);
		return element.isEnabled();
	}

	public static String getElementText(WebElement element) throws IOException, InterruptedException, CloudLeafException {
		waitUntilElement(element);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		waitUntilElement(element);
		highlightElement(element);
		String snapshotName = UIHelper.captureScreenShot("DT");
		test.log(LogStatus.INFO, test.addScreenCapture(snapshotName));
		Thread.sleep(500);
		removeHighlightElement(element);
		return element.getText().trim();
	}

	public static String getAttributeValueOfElement(WebElement element, String attributeName)
			throws IOException, InterruptedException, CloudLeafException {
		waitUntilElement(element);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		highlightElement(element);
		String snapshotName = UIHelper.captureScreenShot("DT");
		test.log(LogStatus.INFO, test.addScreenCapture(snapshotName));
		Thread.sleep(500);
		removeHighlightElement(element);
		return element.getAttribute(attributeName);
	}

	public static List<WebElement> getListOfElements(By object) throws IOException, InterruptedException, CloudLeafException {
		List<WebElement> listOfElements = null;

		listOfElements = driver.findElements(object);

		for (int i = 0; i < listOfElements.size(); i++) {
			isElementDisplayed(listOfElements.get(i));
		}

		return listOfElements;
	}

	public static ArrayList<String> getTextContentOfList(By locator) throws IOException, InterruptedException {
		ArrayList<String> listOfElements = new ArrayList<String>();
		for (int i = 0; i < driver.findElements(locator).size(); i++) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElements(locator).get(i));
			if (driver.findElements(locator).get(i).getText().trim().length() > 0) {
				listOfElements.add(driver.findElements(locator).get(i).getText().trim());
			}
		}

		return listOfElements;
	}

	public static void dragAndDropElement(WebElement source, WebElement target)
			throws IOException, InterruptedException, CloudLeafException {
		UIHelper.isElementDisplayed(target);
		UIHelper.isElementDisplayed(source);
		Actions builder = new Actions(driver);
		Action dragAndDrop = builder.moveToElement(target, 10, 10).pause(2000).clickAndHold(target).pause(2000)
				.moveToElement(target, 10, 100).pause(2000).release(source).build();
		dragAndDrop.perform();
	}

	public static void waitForListSizeGTZero(By object, int length) throws InterruptedException {
		int size;
		for (int i = 0; i < 50; i++) {
			size = driver.findElements(object).size();
			if (size > length) {
				break;
			} else {
				Thread.sleep(2000);
			}
		}

	}

	public static void waitForElementNotDisplayed(By locator) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 100);

			wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
		} catch (Exception e) {

			System.out.println(locator + "not found in the page");
			e.getStackTrace();
		}
		System.out.println(locator + "element found");
		/*
		 * //click the element driver.findElement(By.xpath(xpath)).click();
		 * System.out.println(xpath + "element clicked");
		 */
	}

	public static void waitForTextToBePresent(WebElement locator, String text) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 100);

			wait.until(ExpectedConditions.textToBePresentInElement(locator, text));
		} catch (Exception e) {

			System.out.println(locator + "not found in the page");
			e.getStackTrace();
		}
		System.out.println(locator + "element found");
		/*
		 * //click the element driver.findElement(By.xpath(xpath)).click();
		 * System.out.println(xpath + "element clicked");
		 */
	}

}
