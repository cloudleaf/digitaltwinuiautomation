package com.cloudleaf.automation.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cloudleaf.automation.base.BasePage;
import com.cloudleaf.automation.base.UIHelper;
import com.cloudleaf.automation.exceptions.CloudLeafException;
import com.relevantcodes.extentreports.LogStatus;

public class MappingAdminPage extends BasePage {

	public MappingAdminPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = ".add-button")
	public WebElement addButton;

	@FindBy(css = "[formcontrolname='name']")
	public WebElement locationNameField;

	@FindBy(css = "[formcontrolname='address']")
	public WebElement addressField;

	@FindBy(css = "[type='submit']")
	public WebElement saveButton;

	@FindBy(css = ".mat-snack-bar-container")
	public WebElement bannerMessageContainer;

	@FindBy(css = "[formcontrolname='searchTerm']")
	public WebElement searchBar;

	@FindBy(css = "span.title")
	public WebElement searchResult;

	@FindBy(css = ".main-display h3")
	public WebElement summaryPaneLocationName;

	@FindBy(xpath = "//app-map")
	public WebElement map;

	@FindBy(css = "[title='Zoom in']")
	public WebElement zoomInButton;

	@FindBy(css = "[title='Zoom out']")
	public WebElement zoomOutButton;

	@FindBy(css = ".cdk-virtual-scroll-content-wrapper")
	public WebElement locationsListPane;

	@FindBy(css = ".main-display span p")
	public WebElement summaryPaneAddressDetails;

	@FindBy(css = "app-side-panel-header .fa-times-circle-o")
	public WebElement summaryPaneCloseIcon;

	@FindBy(css = ".input-alert")
	public WebElement alertMessage;
	
	@FindBy(css = ".cdk-virtual-scroll-orientation-vertical")
	public WebElement locationListPane;

	public By locationsList = By.cssSelector("span.title");

	public By summaryPaneLablesList = By.cssSelector(".main-display .css-grid span");

	public void clickAddButton() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click + icon");
		UIHelper.clickElement(addButton);
		UIHelper.waitUntilElement(locationNameField);
	}

	public void enterLocationName(String locationName) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter location name");
		UIHelper.enterText(locationNameField, locationName);
	}

	public void enterLocationAddress(String address) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter location address");
		UIHelper.enterText(addressField, address);
		addressField.sendKeys(Keys.ENTER);
	}

	public void clickSaveButton() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click save button");
		UIHelper.clickElement(saveButton);
		UIHelper.waitUntilElement(bannerMessageContainer);
	}

	public void createLocation(String locationName, String address) throws IOException, InterruptedException, CloudLeafException {
		clickAddButton();
		enterLocationName(locationName);
		enterLocationAddress(address);
	}

	public void enterSearchText(String input) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter search text: " + input + " in search bar");
		UIHelper.enterCharByChar(searchBar, input);
	}
}
