package com.cloudleaf.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cloudleaf.automation.base.BasePage;

public class AssetDetailsPage extends BasePage {

	public AssetDetailsPage() {
		PageFactory.initElements(driver, this);
	}

	public By assetSummaryList = By.cssSelector("app-conditions-panel h4");

	public By assetCustodyList = By.cssSelector("app-custody-conditions h4");

	public By assetDetailsButtonsLabelsList = By.cssSelector("app-asset-details button");
	
	public By custodyActionsList = By.cssSelector("div.custody-list div div div");

	@FindBy(css = "app-chain-of-custody-widget")
	public WebElement chainOfCustodyWidget;
	
	@FindBy(css = "app-chain-of-custody-widget h2")
	public WebElement chainOfCustodyLabel;
	
	@FindBy(css = "[mattooltip='Export chain of custody list as CSV']")
	public WebElement exportLink;
	
	@FindBy(xpath = "//div[text()='Location']")
	public WebElement locationLabel;
	
	@FindBy(xpath = "//span[contains(text(),'Latitude')]")
	public WebElement latitudeLabel;
	
	@FindBy(xpath = "//span[contains(text(),'Longitude')]")
	public WebElement longitudeLabel;
	
	@FindBy(xpath = "//button[contains(text(),'check in')]")
	public WebElement checkinButton;
	
	@FindBy(xpath = "//button[contains(text(),'service')]")
	public WebElement serviceButton;
	
	@FindBy(xpath = "//h4[text()='Condition:']/following::p[1]")
	public WebElement assetConditionLabel;
	
	

}
