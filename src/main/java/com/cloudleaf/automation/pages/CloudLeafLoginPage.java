package com.cloudleaf.automation.pages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cloudleaf.automation.base.BasePage;
import com.cloudleaf.automation.base.UIHelper;
import com.cloudleaf.automation.exceptions.CloudLeafException;
import com.relevantcodes.extentreports.LogStatus;

public class CloudLeafLoginPage extends BasePage {

	@FindBy(css = "[for='username']")
	public WebElement userIDLabel;

	@FindBy(css = "[for='password']")
	public WebElement passwordLabel;

	@FindBy(id = "username")
	public WebElement emailTextField;

	@FindBy(id = "password")
	public WebElement passwordTextField;

	@FindBy(css = "[name='action']")
	public WebElement continueButton;

	@FindBy(id = "login")
	public WebElement loginButton;

	@FindBy(name = "newSecret")
	public WebElement newPasswordTextField;

	@FindBy(name = "newSecretAgain")
	public WebElement newPasswordAgainTextField;

	@FindBy(id = "setPass")
	public WebElement setPasswordButton;

	@FindBy(xpath = "//div[@data-ng-show='showAutoRestPsd']")
	public WebElement resetPasswordText;

	@FindBy(xpath = "(//div[@class='alert alert-danger ng-binding'])[2]")
	public WebElement errorLabel;

	@FindBy(xpath = "//a[contains(text(),'forgot password?')]")
	public WebElement forgotPasswordLink;

	@FindBy(id = "userId")
	public WebElement userIdTextField;

	@FindBy(id = "primaryEmail")
	public WebElement emailIdTextField;

	@FindBy(id = "login")
	public WebElement submitButton;

	@FindBy(xpath = "//div[@data-ng-show='showEmailSentMessage']")
	public WebElement emailSentMsgText;

	@FindBy(css = ".mat-raised-button")
	public WebElement dtLoginButton;

	@FindBy(css = ".account-widget div")
	public WebElement userProfileIcon;

	@FindBy(xpath = "//a[text()='Logout']")
	public WebElement logoutLink;

	@FindBy(css = ".toggle-password")
	public WebElement eyeIcon;

	public By emptyUserNameError = By.cssSelector(".help-block");

	public CloudLeafLoginPage() {
		// this.driver = driver;
		PageFactory.initElements(driver, this);
		dtHomePage = new DigitalTwinHomePage();
	}

	public void login(WebDriver driver, String username, String password) throws IOException, InterruptedException, CloudLeafException {
		System.out.println("Waiting for the email text field to appear");
		UIHelper.waitUntilElement(emailTextField);
		System.out.println("Email Text field displayed");
		emailTextField.sendKeys(username);
		passwordTextField.clear();
		passwordTextField.sendKeys(password);
		loginButton.click();
		dtHomePage.waitForMenuHamburger();

	}

	public void loginDigitalTwin(String username, String password) throws IOException, InterruptedException, CloudLeafException {

		try {
			if (emailTextField.isDisplayed()) {
				enterUserName(username);
				clickContinueButton();
				enterPassword(password);
				clickContinueButton();
				if (username.length() > 0 && password.length() > 0) {
					UIHelper.waitUntilElement(dtHomePage.menuHamburger);
				}
			} else {
				System.out.println("User already logged in...");
			}
		} catch (Exception e) {
			System.out.println("User already logged in...");
		}
	}

	public void clickContinueButton() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click Continue button");
		UIHelper.clickElement(continueButton);
		
	}

	public void resetPassword(WebDriver driver, String newPassword, String newPasswordAgain) throws CloudLeafException {
		UIHelper.waitUntilElement(newPasswordTextField);
		newPasswordTextField.clear();
		newPasswordTextField.sendKeys(newPassword);
		newPasswordAgainTextField.clear();
		newPasswordAgainTextField.sendKeys(newPasswordAgain);
		setPasswordButton.click();
		System.out.println("User Resetted his password successfully");
	}

	public boolean verifyResetPasswordPage(WebDriver driver) {
		UIHelper.waitForPageLoaded();
		return setPasswordButton.getText().trim().equals("Set Password");
	}

	public void forgotPassword(WebDriver driver, String userId, String email) throws InterruptedException, CloudLeafException {
		UIHelper.waitUntilElement(forgotPasswordLink);
		forgotPasswordLink.click();

		UIHelper.waitUntilElement(userIdTextField);
		// Thread.sleep(3000);
		userIdTextField.sendKeys(userId);
		emailIdTextField.sendKeys(email);
		submitButton.click();
		Thread.sleep(3000);
		UIHelper.waitUntilElement(emailSentMsgText);
		System.out.println("User successfully done forgot password flow");
	}

	public void enterUserName(String username) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter UserName");
		UIHelper.enterText(emailTextField, username);
	}

	public void enterPassword(String password) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter Password");
		UIHelper.enterText(passwordTextField, password);
	}

	public void clickSubmitButton() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click Submit button");
		UIHelper.clickElement(dtLoginButton);
	}

	public void logout() throws IOException, InterruptedException, CloudLeafException {
		try {
			if (userProfileIcon.isDisplayed()) {

				test.log(LogStatus.INFO, "Click user profile icon");
				UIHelper.jsClick(userProfileIcon);

				test.log(LogStatus.INFO, "Click logout link");
				UIHelper.jsClick(logoutLink);

				UIHelper.waitUntilElement(emailTextField);

			}
		} catch (NoSuchElementException nse) {

		}
	}

	public List<WebElement> getEmptyCredErrorMessages() throws CloudLeafException {
		UIHelper.waitUntilElement(driver.findElement(emptyUserNameError));
		test.log(LogStatus.INFO, "Get List of Tenants");
		return driver.findElements(emptyUserNameError);
	}

	public boolean isUserIDTextFieldDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if User ID text field is displayed");
		return UIHelper.isElementDisplayed(emailTextField);
	}

	public boolean isPasswordTextFieldDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Password text field is displayed");
		return UIHelper.isElementDisplayed(passwordTextField);
	}

	public boolean isSubmitButtonDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Submit button is displayed");
		return UIHelper.isElementDisplayed(dtLoginButton);
	}

	public boolean isUserIDLabelDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if User ID Label is displayed");
		return UIHelper.isElementDisplayed(userIDLabel);
	}

	public boolean isPasswordLabelDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Password Label is displayed");
		return UIHelper.isElementDisplayed(passwordLabel);
	}

}
