package com.cloudleaf.automation.pages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cloudleaf.automation.base.BasePage;
import com.cloudleaf.automation.base.UIHelper;
import com.cloudleaf.automation.exceptions.CloudLeafException;
import com.relevantcodes.extentreports.LogStatus;

public class DigitalTwinHomePage extends BasePage {

	@FindBy(id = "username")
	public WebElement emailTextField;

	@FindBy(id = "wt__hamburger")
	public WebElement menuHamburger;

	@FindBy(xpath = "//div[text()='DIGITAL MODEL']/preceding::span[1]")
	public WebElement digitalModelOption;

	@FindBy(xpath = "//div[text()='Live View']/preceding::span[1]")
	public WebElement liveViewOption;
	
	@FindBy(xpath = "//div[text()='RULES ENGINE']/preceding::span[1]")
	public WebElement rulesEngineOption;
	
	@FindBy(xpath = "//div[text()='MAPPING ADMIN']")
	public WebElement mappingAdminOption;
	
	@FindBy(xpath = "//div[text()='ASSETS']")
	public WebElement manageAssetsOption;
	
	@FindBy(xpath = "//div[text()='ADMIN']")
	public WebElement adminOption;

	@FindBy(className = "wt__info-cards")
	public WebElement liveViewTopHeader;

	@FindBy(css = ".wt__info-card .fa-angle-down")
	public WebElement changeTenantdropDown;

	@FindBy(css = ".mat-menu-content")
	public WebElement tenantDropdownList;

	@FindBy(css = ".capitalize.justify-self-end")
	public WebElement tenantHeader;

	@FindBy(css = ".header-grid img")
	public WebElement cloudLeafLogo;
	
	@FindBy(css = ".logo img")
	public WebElement clientLogo;
	
	@FindBy(linkText = "Users")
	public WebElement UsersLink;

	public By listOfTenants = By.cssSelector(".mat-menu-content li");

	public By menuOptions = By.cssSelector("[role='menu'] li .font-thin");

	public DigitalTwinHomePage() {
		// this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void waitForMenuHamburger() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Wait for Hamberger menu");

		UIHelper.waitUntilElement(menuHamburger);
	}

	public void clickMenuHamburger() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click Hamberger menu");

		UIHelper.clickElement(menuHamburger);
	}

	public void clickLiveViewOption() throws IOException, InterruptedException, CloudLeafException {
		UIHelper.clickElement(liveViewOption);
	}

	public void clickDigitalModelOption() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click Digital Model option");
		UIHelper.clickElement(digitalModelOption);
	}
	
	public void clickAdminOption() throws IOException, InterruptedException, CloudLeafException
	{
		test.log(LogStatus.INFO, "Click Admin option");
		UIHelper.clickElement(adminOption);
	}

	public void clickChangeTenantdropDown() throws IOException, InterruptedException, CloudLeafException {

		test.log(LogStatus.INFO, "Click Change Tenant dropdown");
		UIHelper.clickElement(changeTenantdropDown);
	}

	public List<WebElement> getListOfTenants() throws CloudLeafException {
		UIHelper.waitUntilElement(tenantDropdownList);
		test.log(LogStatus.INFO, "Get List of Tenants");
		return driver.findElements(listOfTenants);
	}

	public void selectTenant(String tenantName) {
		for (int i = 0; i < driver.findElements(listOfTenants).size(); i++) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElements(listOfTenants).get(i));
			if (driver.findElements(listOfTenants).get(i).getText().trim().equals(tenantName)) {
				UIHelper.highlightElement(driver.findElements(listOfTenants).get(i));
				driver.findElements(listOfTenants).get(i).click();
				break;
			}
		}
	}

	public boolean isCloudLeafLogoDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if CloudLeaf logo is displayed");
		return UIHelper.isElementDisplayed(cloudLeafLogo);

	}
	
	public boolean isClientLogoDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Client logo is displayed");
		return UIHelper.isElementDisplayed(clientLogo);

	}

	public List<WebElement> getMenuOptions() {
		test.log(LogStatus.INFO, "Get Menu Options");
		return driver.findElements(menuOptions);
	}
	
	public boolean isRulesEngineOptionDisplayed() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.isElementDisplayed(rulesEngineOption);
	}
	
	public void clickRulesEngineOption() throws IOException, InterruptedException, CloudLeafException
	{
		test.log(LogStatus.INFO, "Click Rules engine link");
		UIHelper.clickElement(rulesEngineOption);
	}
	
	public void clickManageAssetsOption() throws IOException, InterruptedException, CloudLeafException
	{
		test.log(LogStatus.INFO, "Click Assets link");
		UIHelper.clickElement(manageAssetsOption);
	}
	
	public void clickMappingAdminOption() throws IOException, InterruptedException, CloudLeafException
	{
		test.log(LogStatus.INFO, "Click Mapping admin link");
		UIHelper.clickElement(mappingAdminOption);
	}
	
	public void clickUsersLink() throws IOException, InterruptedException, CloudLeafException
	{
		test.log(LogStatus.INFO, "Click Users link");
		UIHelper.clickElement(UsersLink);
	}

}
