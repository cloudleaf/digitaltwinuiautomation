package com.cloudleaf.automation.pages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cloudleaf.automation.base.UIHelper;
import com.cloudleaf.automation.exceptions.CloudLeafException;
import com.relevantcodes.extentreports.LogStatus;

public class ManageDigitalTwinPage extends UIHelper {

	public ManageDigitalTwinPage() {
		// this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "md-add-button")
	public WebElement plusIcon;

	@FindBy(xpath = "//a[text()='Add new Items']")
	public WebElement addNewItemsOption;

	@FindBy(xpath = "//li[contains(text(),'Create')]")
	public WebElement createLink;

	@FindBy(xpath = "//li[contains(text(),'List')]")
	public WebElement ListLink;

	@FindBy(xpath = "//li[contains(text(),'Configure')]")
	public WebElement configureLink;

	@FindBy(xpath = "//li[text()='Map']")
	public WebElement mapLink;

	@FindBy(css = "[formcontrolname='name']")
	public WebElement entityNameField;

	@FindBy(css = "app-entity-creator [name='entity']")
	public WebElement newEntityTypeDropdown;

	@FindBy(css = "[value='entities']")
	public WebElement entitiesRadioButton;

	@FindBy(css = "[value='relationships']")
	public WebElement relationshipsRadioButton;

	@FindBy(css = "[mattooltip='Create Entity']")
	public WebElement createButton;

	@FindBy(css = "app-md-create-main mat-label")
	public WebElement selectTypeDropdown;

	@FindBy(css = ".create-container h2")
	public WebElement buildDTLabel;

	@FindBy(css = "app-entity-list")
	public WebElement entitiesTable;

	@FindBy(css = ".mat-bottom-sheet-container")
	public WebElement uploadedEntitiesDialog;

	@FindBy(xpath = "//a[text()='Select Files']")
	public WebElement uploadCSVLink;

	@FindBy(css = "app-inspector")
	public WebElement shipmentDetailsPanel;

	@FindBy(css = "app-inspector h3")
	public WebElement shipmentDetailsPanelHeader;

	@FindBy(css = "app-inspector .summary-icon")
	public WebElement shipmentDetailsPanelSummaryIcon;

	@FindBy(css = "app-inspector [mattooltip='Close panel']")
	public WebElement shipmentDetailsPanelCloseIcon;

	@FindBy(css = "#app-graph-map")
	public WebElement configurePageCanvas;

	@FindBy(css = ".sebm-google-map-container")
	public WebElement mapPagecontainer;

	@FindBy(css = ".input-alert")
	public WebElement alertMessage;

	public By entityTypeOptions = By.cssSelector(".mat-select-panel .mat-option-text");

	public static By customEntitiesList = By.xpath("//h4[text()='Custom Entities']/following::ul[1]//li");

	public By topMenuTabsList = By.cssSelector(".sub-header-grid li");

	public By createDTMenuLabelsList = By.cssSelector(".mat-card h3");

	public By createNewEntityMenuLabelsList = By.cssSelector("app-entity-creator label");

	public By importDataMenuLabels = By.cssSelector("mat-radio-group label");

	public By entitiesList = By.cssSelector("app-entity-list a");

	public By importDataEntityTypeOptions = By.cssSelector("#mat-select-1-panel .mat-option-text");

	public By leftMenuEntitiesList = By.cssSelector(".entity-sidebar-grid app-nav-item .list-drag-item");

	public By leftMenuEntitiesHeaders = By.cssSelector(".entity-sidebar-grid .h5");

	public By downArrowIcon = By.cssSelector(".entity-sidebar-grid .fa-angle-right.active");

	public void clickPlusIcon() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click + icon");
		UIHelper.clickElement(plusIcon);
	}

	public void clickCreateLink() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click create link");
		UIHelper.clickElement(createLink);
	}

	public void clickAddNewItemsOption() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click add new items option");
		UIHelper.clickElement(addNewItemsOption);
	}

	public void enterEntityName(String input) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter entity name");
		UIHelper.enterText(entityNameField, input);
	}

	public void clickNewEntityTypeDropdown() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click entity type dropdown");
		UIHelper.clickElement(newEntityTypeDropdown);
	}

	public void selectEntityType(String option) {
		test.log(LogStatus.INFO, "Select entity type from entity type dropdown");
		for (int i = 0; i < driver.findElements(entityTypeOptions).size(); i++) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElements(entityTypeOptions).get(i));
			if (driver.findElements(entityTypeOptions).get(i).getText().equals(option)) {
				UIHelper.highlightElement(driver.findElements(entityTypeOptions).get(i));
				driver.findElements(entityTypeOptions).get(i).click();
				newEntityTypeDropdown.sendKeys(Keys.TAB);
				break;
			}
		}
	}

	public void clickCreateButton() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Select entity type from entity type dropdown");
		UIHelper.clickElement(createButton);
	}

	public boolean verifyCustomEntities(String name) {
		boolean flag = false;
		System.out.println("Verify " + name + " from the custom entities list");
		test.log(LogStatus.INFO, "Verify " + name + " from the custom entities list");
		for (int i = 0; i < driver.findElements(customEntitiesList).size(); i++) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElements(customEntitiesList).get(i));
			if (driver.findElements(customEntitiesList).get(i).getText().trim().equals(name)) {
				UIHelper.highlightElement(driver.findElements(customEntitiesList).get(i));
				flag = true;
				break;
			}
		}

		return flag;
	}

	public void selectEntitiesRadioButton() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Select Entities Radio Button");
		UIHelper.clickElement(entitiesRadioButton);
	}

	public void selectRelationshipsRadioButton() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Select Entities Radio Button");
		UIHelper.clickElement(relationshipsRadioButton);
	}

	public List<WebElement> getTopMenuTabsList() {
		test.log(LogStatus.INFO, "Get Top Menu Tabs list");
		return driver.findElements(topMenuTabsList);
	}

	public List<WebElement> getCreateDTMenuLabelsList() {
		test.log(LogStatus.INFO, "Get Create Digital Twin menu labels list");
		return driver.findElements(createDTMenuLabelsList);
	}

	public List<WebElement> getCreateNewEntityMenuLabelsList() {
		test.log(LogStatus.INFO, "Get Create New Entity menu labels list");
		return driver.findElements(createNewEntityMenuLabelsList);
	}

	public boolean isEntityNameFieldDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Entity Name Textfield displayed");
		return UIHelper.isElementDisplayed(entityNameField);
	}

	public boolean isEntityTypeDropdownDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Entity Type Dropdown displayed");
		return UIHelper.isElementDisplayed(newEntityTypeDropdown);
	}

	public boolean isCreateButtonDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Create Button displayed");
		return UIHelper.isElementDisplayed(createButton);
	}

	public boolean isEntitiesRadioButtonDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Entities Radio Button displayed");
		return UIHelper.isElementDisplayed(entitiesRadioButton);
	}

	public boolean isRelationshipsRadioButtonDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Relationships Radio Button displayed");
		return UIHelper.isElementDisplayed(relationshipsRadioButton);
	}

	public boolean isSelectEntityTypeDropdownDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Select Entity type Dropdown Button displayed");
		return UIHelper.isElementDisplayed(selectTypeDropdown);
	}

	public List<WebElement> getImportDataMenuLabels() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "get import data menu labels");
		List<WebElement> labelsList = driver.findElements(importDataMenuLabels);
		for (int i = 0; i < labelsList.size(); i++) {
			UIHelper.isElementDisplayed(labelsList.get(i));
		}
		return labelsList;
	}

	public void clickListLink() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click List link");
		UIHelper.clickElement(ListLink);
	}

	public List<WebElement> getEntitiesList() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "get list of entities in lister page");
		List<WebElement> listOfEntities = driver.findElements(entitiesList);
		for (int i = 0; i < listOfEntities.size(); i++) {
			UIHelper.isElementDisplayed(listOfEntities.get(i));
		}
		return listOfEntities;
	}

	public void clickSelectTypeDropdown() throws IOException, InterruptedException, CloudLeafException {
		UIHelper.clickElement(selectTypeDropdown);
	}

	public void selectImportDataEntityTypeOption(String option)
			throws IOException, InterruptedException, CloudLeafException {
		List<WebElement> list_Options = driver.findElements(importDataEntityTypeOptions);
		for (int i = 0; i < list_Options.size(); i++) {
			if (list_Options.get(i).getText().trim().equals(option)) {
				UIHelper.isElementDisplayed(list_Options.get(i));
				list_Options.get(i).click();
				break;
			}
		}
	}

	public void clickOnAssetLink() throws IOException, InterruptedException, CloudLeafException {

		UIHelper.clickElement(driver.findElements(entitiesList).get(0));

	}

	public void uploadCSVFile(String filePath) throws IOException, InterruptedException, CloudLeafException {
		UIHelper.enterText(uploadCSVLink, filePath);
	}

	public boolean isuploadedEntitiesDialogDisplayed() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.isElementDisplayed(uploadedEntitiesDialog);
	}

	public List<WebElement> getListPageLeftMenuHeaders() {
		return driver.findElements(By.tagName("h4"));
	}

	public boolean isShipmentDetailsPanelDisplayed() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.isElementDisplayed(shipmentDetailsPanel);
	}

	public boolean isShipmentDetailsPanelHeaderDisplayed() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.isElementDisplayed(shipmentDetailsPanelHeader);
	}

	public boolean isShipmentDetailsPanelSummaryIconDisplayed() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.isElementDisplayed(shipmentDetailsPanelSummaryIcon);
	}

	public boolean isShipmentDetailsPanelCloseIconDisplayed() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.isElementDisplayed(shipmentDetailsPanelCloseIcon);
	}

	public void clickConfigureLink() throws IOException, InterruptedException, CloudLeafException {
		UIHelper.clickElement(configureLink);
		;
	}

	public boolean isConfigurePageCanvasDisplayed() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.isElementDisplayed(configurePageCanvas);
	}

	public List<WebElement> getleftMenuListOfEntities() {
		return driver.findElements(leftMenuEntitiesList);
	}

	public boolean isDraggableEntitiesDisplayed() {
		boolean flag = false;
		for (int i = 0; i < getleftMenuListOfEntities().size(); i++) {
			flag = getleftMenuListOfEntities().get(i).findElement(By.cssSelector(".list-drag-item")).isDisplayed();
		}

		return flag;
	}

	public List<WebElement> getListOfEntitiesHeaders() throws IOException, InterruptedException, CloudLeafException {
		List<WebElement> list_DownArrowIcon = UIHelper.getListOfElements(downArrowIcon);
		if (list_DownArrowIcon.size() > 0) {
			for (int i = 0; i < list_DownArrowIcon.size(); i++) {
				UIHelper.clickElement(list_DownArrowIcon.get(i));
			}
		}
		return UIHelper.getListOfElements(leftMenuEntitiesHeaders);
	}

	public void clickMapLink() throws IOException, InterruptedException, CloudLeafException {
		UIHelper.clickElement(mapLink);
		;
	}

	public boolean isMapPagecontainerDisplayed() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.isElementDisplayed(mapPagecontainer);
	}

	public List<WebElement> getCustomEntitiesList() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.getListOfElements(customEntitiesList);
	}

	public boolean isAlertMessageDisplayed() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.isElementDisplayed(alertMessage);
	}

	public String getAlertMessage() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.getElementText(alertMessage);
	}

}
