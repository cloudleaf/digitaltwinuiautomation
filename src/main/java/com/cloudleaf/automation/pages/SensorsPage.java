package com.cloudleaf.automation.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cloudleaf.automation.base.BasePage;
import com.cloudleaf.automation.base.UIHelper;
import com.cloudleaf.automation.exceptions.CloudLeafException;
import com.cloudleaf.automation.utils.Utils;
import com.relevantcodes.extentreports.LogStatus;

public class SensorsPage extends BasePage {

	public SensorsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = ".add-button")
	public WebElement addButton;

	@FindBy(css = "[formcontrolname='id']")
	public WebElement sensorIDField;

	@FindBy(css = ".mat-select-value")
	public WebElement sensorTypeDropdown;

	@FindBy(css = "[type='submit']")
	public WebElement saveButton;

	@FindBy(linkText = "Sensors")
	public WebElement sensorsLink;

	@FindBy(xpath = "//app-sensor-list //button[contains(text(),'unbind')]")
	public WebElement unbindButton;

	@FindBy(css = "app-manage-inspector")
	public WebElement bindSensorPanel;

	@FindBy(css = "app-manage-inspector .panel-header")
	public WebElement bindSensorPanelHeader;

	@FindBy(css = "app-manage-inspector [formcontrolname='searchTerm']")
	public WebElement assetsSearchField;

	@FindBy(xpath = "//*[text()='Cancel']")
	public WebElement cancelButton;

	@FindBy(css = "button.mat-primary")
	public WebElement bindSensorPanelBindButton;
	
	@FindBy(xpath = "//div[contains(@class,'panel')]//*[contains(text(),'Status')]/following::mat-checkbox[1]/label")
	public WebElement boundCheckbox;

	@FindBy(xpath = "//div[contains(@class,'panel')]//*[contains(text(),'Status')]/following::mat-checkbox[2]/label")
	public WebElement unBoundCheckbox;

	public By loaderIcon = By.cssSelector(".fa-spinner");

	public By listOfSensorTypes = By.cssSelector("[role='listbox'] mat-option");

	public By columnHeadersList = By.cssSelector(".list-header span");

	public By sensorsTitlesList = By.cssSelector("cdk-virtual-scroll-viewport span.grid");

	public By sensorTypeCheckboxesList = By.xpath("//nav[contains(@class,'filter-list')]//mat-checkbox/label");

	public void clickAddButton() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click add button");
		UIHelper.clickElement(addButton);
	}

	public void enterSensorID(String sensorID) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter Sensor ID");
		UIHelper.enterText(sensorIDField, sensorID);
	}

	public void selectSensorType() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Select sensor type");
		UIHelper.clickElement(sensorTypeDropdown);
		int length = driver.findElements(listOfSensorTypes).size();
		int indexToSelect = Utils.generateRandomNumberInRange(length);
		UIHelper.highlightElement(driver.findElements(listOfSensorTypes).get(indexToSelect));
		driver.findElements(listOfSensorTypes).get(indexToSelect).click();
	}

	public void clickSaveButton() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click Save button");
		UIHelper.clickElement(saveButton);
	}

	public void createSensor(String sensorID) throws IOException, InterruptedException, CloudLeafException {
		clickAddButton();
		enterSensorID(sensorID);
		selectSensorType();
		clickSaveButton();
		UIHelper.waitForElementNotDisplayed(loaderIcon);
	}

	public ArrayList<String> getColumnHeadersList() {
		ArrayList<String> columnHeaders = new ArrayList<String>();

		for (int i = 0; i < driver.findElements(columnHeadersList).size(); i++) {

			if (driver.findElements(columnHeadersList).get(i).getText().trim().length() > 0) {
				columnHeaders.add(driver.findElements(columnHeadersList).get(i).getText());
			}

		}

		return columnHeaders;
	}

	public List<WebElement> getSensorTypeCheckboxesList() {
		List<WebElement> list = driver.findElements(sensorTypeCheckboxesList);
		List<WebElement> listOfCheckboxes = new ArrayList<WebElement>();;
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).getText().contains("Bio")||list.get(i).getText().contains("Nlk"))
			{
				listOfCheckboxes.add(list.get(i));
			}
		}
		return listOfCheckboxes;
	}

}
