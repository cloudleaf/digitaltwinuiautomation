package com.cloudleaf.automation.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cloudleaf.automation.base.BasePage;
import com.cloudleaf.automation.base.UIHelper;
import com.cloudleaf.automation.exceptions.CloudLeafException;
import com.cloudleaf.automation.utils.Utils;
import com.relevantcodes.extentreports.LogStatus;

public class AssetsPage extends BasePage {

	public AssetsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='DIGITAL MODEL']/preceding::span[1]")
	public WebElement digitalModelOption;

	@FindBy(css = "[formcontrolname='searchTerm']")
	public WebElement searchBar;

	@FindBy(xpath = "//app-entity-list //button[contains(text(),'edit ')]")
	public WebElement editButton;

	@FindBy(xpath = "//app-entity-list //button[contains(text(),'bind')]")
	public WebElement bindButton;

	@FindBy(xpath = "//app-entity-list //button[contains(text(),'unbind')]")
	public WebElement unbindButton;

	@FindBy(xpath = "//app-entity-list //button[contains(text(),'bulk bind')]")
	public WebElement bulkbindButton;

	@FindBy(xpath = "//app-entity-list //button[contains(text(),'bulk unbind')]")
	public WebElement bulkunbindButton;

	@FindBy(xpath = "//app-assets-filter //*[@class='panel-header-grid']")
	public WebElement assetCategorySection;

	@FindBy(xpath = "//app-list-filter//div//h5")
	public WebElement filterBySection;

	@FindBy(css = ".add-button")
	public WebElement addButton;

	@FindBy(css = "[formcontrolname='assetId']")
	public WebElement assetIDField;

	@FindBy(css = "[formcontrolname='name']")
	public WebElement assetNameField;

	@FindBy(css = "[formcontrolname='qrCode']")
	public WebElement qrCodeField;

	@FindBy(css = "[formcontrolname='sku']")
	public WebElement assetSKUField;

	@FindBy(css = ".dropdown-bg")
	public WebElement assetCategoryDropdown;

	@FindBy(css = "[formcontrolname='cost']")
	public WebElement costField;

	@FindBy(xpath = "//div[contains(text(),'Good')]")
	public WebElement goodConditionRadioButton;

	@FindBy(xpath = "//div[contains(text(),'Damaged')]")
	public WebElement damagedConditionRadioButton;

	@FindBy(css = "[formcontrolname='manufacturer']")
	public WebElement manufacturerField;

	@FindBy(css = "[formcontrolname='warehouseLocation']")
	public WebElement warehouseLocationField;

	@FindBy(css = "[formcontrolname='maintenanceDate']")
	public WebElement maintenanceDateField;

	@FindBy(css = "[type='submit']")
	public WebElement saveButton;

	@FindBy(linkText = "ASSET DETAILS")
	public WebElement assetDetailsLink;

	@FindBy(css = ".mat-dialog-content p")
	public WebElement confirmWindowMessage;

	@FindBy(css = "app-asset-binder [formcontrolname='searchTerm']")
	public WebElement bindAssetSearchbar;

	@FindBy(xpath = "//span[contains(text(),'Bind')]")
	public WebElement assetBindButton;

	@FindBy(linkText = "LOAD NEXT 50 ITEMS")
	public WebElement loadNext50ItemsLink;

	@FindBy(css = "app-category-manager .fa-plus")
	public WebElement assetCategoryAddButton;

	@FindBy(css = "[formcontrolname='name']")
	public WebElement assetCategoryNameField;

	@FindBy(css = "app-category-manager .fa-trash")
	public WebElement assetCategoryDeleteIcon;

	@FindBy(xpath = "//h4[text()='Status']")
	public WebElement statusFilterLabel;

	@FindBy(xpath = "//*[text()=' of 3 Assets']")
	public WebElement assetDisplayCountHeader;

	@FindBy(xpath = "//button[contains(text(),'check out')]")
	public WebElement checkoutButton;

	@FindBy(css = "[placeholder='Type to search']")
	public WebElement fieldTechSearchBar;

	@FindBy(xpath = "//button[contains(text(),'ENTER')]")
	public WebElement enterButton;

	@FindBy(css = "app-checkout-transfer-confirm-dialog button")
	public WebElement checkoutConfirmButton;

	public By loaderIcon = By.cssSelector(".fa-spinner");

	public By listOfAssets = By.cssSelector(".scroll-list-container li");

	public By listOfassetCategories = By.cssSelector("[role='listbox'] mat-option");

	public By assetSummaryList = By.cssSelector("app-conditions-panel div p");

	public By assetCustodyList = By.cssSelector("app-custody-conditions div p");

	public By checkBoxesList = By.cssSelector(".grid-list .mat-checkbox-inner-container");

	public By unbindButtonsList = By.xpath("//app-entity-list //button[contains(text(),'unbind')]");

	public By assetCategoryList = By.cssSelector("app-category-manager span.mat-checkbox-label");

	public By filterCheckboxesList = By.cssSelector(".filter-list .mat-checkbox-label");

	public By assetsStateList = By.xpath("//li[contains(@class,'grid-list')]//span[5]");

	public By assetsStatusList = By.xpath("//li[contains(@class,'grid-list')]//span[6]");

	public By assetsRowCount = By.xpath("//li[contains(@class,'grid-list')]");

	public By checkoutButtonsList = By.cssSelector(".checkout-buttons button");

	public boolean isSearchBarDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Search bar is displayed");
		return UIHelper.isElementDisplayed(searchBar);
	}

	public boolean isUnbindButtonDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Unbind button is displayed");
		return UIHelper.isElementDisplayed(unbindButton);
	}

	public boolean isDeleteButtonDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Delete button is displayed");
		return UIHelper.isElementDisplayed(commonPage.deleteButton);
	}

	public boolean isBulkBindButtonDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Bulk bind button is displayed");
		return UIHelper.isElementDisplayed(bulkbindButton);
	}

	public boolean isBulkUnbindButtonDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Bulk unbind is displayed");
		return UIHelper.isElementDisplayed(bulkunbindButton);
	}

	public boolean isAssetCategorySectionDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Asset category section is displayed");
		return UIHelper.isElementDisplayed(assetCategorySection);
	}

	public boolean isFilterBySectionDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Filter by section is displayed");
		return UIHelper.isElementDisplayed(filterBySection);

	}

	public boolean AreListOfAssetsDisplayed() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if list of assets are displayed");
		return driver.findElements(listOfAssets).size() > 0;
	}

	public void clickPlusIcon() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click plus icon");
		UIHelper.clickElement(addButton);
	}

	public void enterAssetID(String assetID) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter Asset ID");
		UIHelper.enterText(assetIDField, assetID);
	}

	public void enterAssetName(String assetName) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter Asset Name");
		UIHelper.enterText(assetNameField, assetName);
	}

	public void enterQRCode(String qrCode) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter QR Code");
		UIHelper.enterText(qrCodeField, qrCode);
	}

	public void enterAssetSKUCode(String assetSKU) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter Asset SKU");
		UIHelper.enterText(assetSKUField, assetSKU);
	}

	public void selectAssetCategory() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Select Asset Category");
		UIHelper.clickElement(assetCategoryDropdown);
		List<WebElement> listOfOptions = driver.findElements(listOfassetCategories);
		int length = driver.findElements(listOfassetCategories).size();
		int indexToSelect = Utils.generateRandomNumberInRange(length);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
				listOfOptions.get(indexToSelect));
		UIHelper.highlightElement(listOfOptions.get(indexToSelect));
		listOfOptions.get(indexToSelect).click();

	}

	public void enterAssetCost(int assetCost) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter Asset Cost");
		UIHelper.enterText(costField, String.valueOf(assetCost));
	}

	public void selectAssetGoodCondition() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Select Asset Condition - Good");
		UIHelper.clickElement(goodConditionRadioButton);
	}

	public void selectAssetDamagedCondition() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Select Asset Condition - Damaged");
		UIHelper.clickElement(damagedConditionRadioButton);
	}

	public void enterManufacturerName(String mfgName) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter Manufacturer/Vendor name");
		UIHelper.enterText(manufacturerField, mfgName);
	}

	public void enterWarehouseLocation(String warehouseLocation) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter Warehouse location");
		UIHelper.enterText(warehouseLocationField, warehouseLocation);
	}

	public void enterMaintenanceDate(String maintenanceDate) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter Maintenance Date");
		UIHelper.enterText(maintenanceDateField, maintenanceDate);
	}

	public void clickSaveButton() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click Save button");
		UIHelper.clickElement(saveButton);
	}

	public void enterSearchText(String searchText) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter search text in Search bar");
		UIHelper.pressBackspaceKey(searchBar);
		UIHelper.enterCharByChar(searchBar, searchText);
	}

	public void clickAssetDetailsLink() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click Asset Details button");
		UIHelper.clickElement(assetDetailsLink);
	}

	public List<WebElement> getAssetSummaryList() {
		return driver.findElements(assetSummaryList);
	}

	public List<WebElement> getAssetCustodyList() {
		return driver.findElements(assetCustodyList);
	}

	public List<WebElement> getCheckBoxesList() {
		return driver.findElements(checkBoxesList);
	}

	public List<WebElement> getListOfAssets() {
		return driver.findElements(listOfAssets);
	}

	public void bindAssetWithSensor(String sensorID) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter sensor ID in bind asset search bar");
		UIHelper.enterCharByChar(bindAssetSearchbar, sensorID);
		UIHelper.clickElement(commonPage.searchResult);
		clickAssetBindButton();
		Thread.sleep(2000);
	}

	public String getConfirmWindowMessage() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Get Confirm window message content");
		return UIHelper.getElementText(confirmWindowMessage);
	}

	public void clickAssetBindButton() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click Asset bind button");
		UIHelper.clickElement(assetBindButton);
	}

	public void createAsset(String assetID, String qrCode, String assetSKUCode, int assetCost)
			throws IOException, InterruptedException, CloudLeafException {

		clickPlusIcon();

		enterAssetID(assetID);

		enterQRCode(qrCode);

		enterAssetSKUCode(assetSKUCode);

		selectAssetCategory();

		enterAssetCost(assetCost);

		selectAssetGoodCondition();

		clickSaveButton();
		commonPage.waitUntilLoaderIconNotVisible();
	}

	public void createAsset(String assetID, String qrCode, String assetSKUCode, String assetCategory, int assetCost)
			throws IOException, InterruptedException, CloudLeafException {

		clickPlusIcon();

		enterAssetID(assetID);

		enterQRCode(qrCode);

		enterAssetSKUCode(assetSKUCode);

		UIHelper.clickElement(assetCategoryDropdown);
		WebElement option = driver
				.findElement(By.xpath("//div[@role='listbox'] //span[contains(text(),'" + assetCategory + "')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", option);
		UIHelper.highlightElement(option);
		option.click();

		enterAssetCost(assetCost);

		selectAssetGoodCondition();

		clickSaveButton();

		System.out.println(commonPage.getBannerMessageContent());
	}

	public void createAsset(String assetID, String qrCode, String assetSKUCode, int assetCost, String mfgName,
			String warehouseLocation, String maintenanceDate) throws IOException, InterruptedException, CloudLeafException {

		clickPlusIcon();

		enterAssetID(assetID);

		enterQRCode(qrCode);

		enterAssetSKUCode(assetSKUCode);

		selectAssetCategory();

		enterAssetCost(assetCost);

		selectAssetGoodCondition();

		enterManufacturerName(mfgName);

		enterWarehouseLocation(warehouseLocation);

		enterMaintenanceDate(maintenanceDate);

		clickSaveButton();
	}

	public void clickAssetCategoryAddButton() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click Asset Category button");
		UIHelper.clickElement(assetCategoryAddButton);
	}

	public void enterAssetCategoryName(String assetCategory) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter Asset Category Name");
		UIHelper.enterText(assetCategoryNameField, assetCategory);
	}

	public ArrayList<String> getassetCateogyListContent() {
		ArrayList<String> listOfValues = new ArrayList<String>();

		for (int i = 0; i < driver.findElements(assetCategoryList).size(); i++) {
			listOfValues.add(driver.findElements(assetCategoryList).get(i).getText());
		}

		return listOfValues;
	}

	public void deleteAssetCategory(String assetCategory) throws IOException, InterruptedException, CloudLeafException {
		Thread.sleep(2000);
		WebElement checkbox = driver
				.findElement(By.xpath("//span[text()='" + assetCategory + "']/preceding::input[1]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", checkbox);
		Actions actions = new Actions(driver);
		actions.moveToElement(checkbox).click().build().perform();
		UIHelper.clickElement(assetCategoryDeleteIcon);
		UIHelper.clickElement(commonPage.confirmWindowYesButton);
	}

	public void selectAssetCategoryInLeftPane(String assetCategory) throws IOException, InterruptedException, CloudLeafException {
		Thread.sleep(2000);
		WebElement checkbox = driver
				.findElement(By.xpath("//span[text()='" + assetCategory + "']/preceding::input[1]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", checkbox);
		Actions actions = new Actions(driver);
		actions.moveToElement(checkbox).click().build().perform();
	}

	public List<WebElement> getFilterCheckboxesList() {
		return driver.findElements(filterCheckboxesList);
	}

	public ArrayList<String> getAssetCategoryLabelList() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.getTextContentOfList(assetCategoryList);
	}

	public ArrayList<String> getAssetsStateList() {
		ArrayList<String> listOfValues = new ArrayList<String>();

		for (int i = 1; i < driver.findElements(assetsStateList).size(); i++) {
			listOfValues.add(driver.findElements(assetsStateList).get(i).getText().trim());
		}

		return listOfValues;
	}

	public ArrayList<String> getAssetsStatusList() {
		ArrayList<String> listOfValues = new ArrayList<String>();

		for (int i = 1; i < driver.findElements(assetsStatusList).size(); i++) {
			listOfValues.add(driver.findElements(assetsStatusList).get(i).getText().trim());
		}

		return listOfValues;
	}

	public int getAssetsRowCount() {
		return driver.findElements(assetsRowCount).size() - 1;
	}

	public ArrayList<String> getAssetsDetailsList() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.getTextContentOfList(assetSummaryList);
	}

	public ArrayList<String> getAssetsCustodyDetailsListList() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.getTextContentOfList(assetCustodyList);
	}

	public ArrayList<String> getCheckoutButtonsList() throws IOException, InterruptedException, CloudLeafException {
		ArrayList<String> listOfElementsText = new ArrayList<String>();

		for (int i = 0; i < driver.findElements(checkoutButtonsList).size(); i++) {
			if (driver.findElements(checkoutButtonsList).get(i).isEnabled()) {
				listOfElementsText.add(driver.findElements(checkoutButtonsList).get(i).getText().trim());
			}
		}

		return listOfElementsText;
	}

	public boolean isCheckoutButtonDisplayedAndEnabled() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Check if Checkout button is displayed and enabled");
		return (UIHelper.isElementDisplayed(checkoutButton) && UIHelper.isElementEnabled(checkoutButton));
	}

}
