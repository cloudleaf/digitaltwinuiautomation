package com.cloudleaf.automation.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cloudleaf.automation.base.BasePage;
import com.cloudleaf.automation.base.UIHelper;
import com.cloudleaf.automation.exceptions.CloudLeafException;
import com.relevantcodes.extentreports.LogStatus;

public class CommonPage extends BasePage {

	public CommonPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "[formcontrolname='searchTerm']")
	public WebElement searchBar;

	public By loaderIcon = By.cssSelector(".fa-spinner");

	@FindBy(css = ".search-list-item")
	public WebElement searchResult;

	@FindBy(css = "app-confirm-dialog p")
	public WebElement confirmDialogMessage;

	@FindBy(css = ".mat-dialog-actions .mat-primary")
	public WebElement confirmWindowYesButton;

	@FindBy(css = ".mat-snack-bar-container")
	public WebElement bannerMessageContainer;

	@FindBy(xpath = "//button[contains(text(),'bind')]")
	public WebElement bindButton;

	@FindBy(xpath = "//button[contains(text(),'replace')]")
	public WebElement replaceButton;

	@FindBy(xpath = "//button[contains(text(),'delete')]")
	public WebElement deleteButton;
	
	@FindBy(xpath = "//button[contains(text(),'edit')]")
	public WebElement editButton;

	@FindBy(css = ".mat-bottom-sheet-container")
	public WebElement popupMessageContainer;

	public By popMessageAssetsList = By.cssSelector(".mat-bottom-sheet-container span");

	public By checkBoxesList = By.cssSelector(".grid-list .mat-checkbox-inner-container");

	public By dataColumnsList = By.cssSelector(".cdk-virtual-scroll-content-wrapper span.grid");
	
	public List<WebElement> getCheckBoxesList() {
		return driver.findElements(checkBoxesList);
	}

	public void enterSearchText(String searchText) throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Enter search text in Search bar");
		UIHelper.pressBackspaceKey(searchBar);
		UIHelper.enterCharByChar(searchBar, searchText);
	}

	public List<WebElement> getColumnsList() {
		return driver.findElements(dataColumnsList);
	}
	
	public ArrayList<String> getColumnsListData() throws IOException, InterruptedException, CloudLeafException {
		return UIHelper.getTextContentOfList(dataColumnsList);
	}

	public void waitUntilLoaderIconNotVisible() {
		UIHelper.waitForElementNotDisplayed(loaderIcon);
	}

	public void selectMultipleCheckboxes(ArrayList<String> dataList) throws InterruptedException, IOException {

		waitUntilLoaderIconNotVisible();

		for (int i = 0; i < dataList.size(); i++) {
			driver.findElement(By.xpath("//*[text()='" + dataList.get(i) + "']/preceding::mat-checkbox[1]/label"))
					.click();
		}

	}

	public ArrayList<String> getPopMessageAssetsListContent() {
		ArrayList<String> listOfValues = new ArrayList<String>();

		for (int i = 0; i < driver.findElements(popMessageAssetsList).size(); i++) {
			listOfValues.add(driver.findElements(popMessageAssetsList).get(i).getText());
		}

		return listOfValues;
	}

	public void clickConfirmWindowYesButton() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Click Yes button in confirm window");
		UIHelper.clickElement(confirmWindowYesButton);
	}
	
	public String getBannerMessageContent() throws IOException, InterruptedException, CloudLeafException {
		test.log(LogStatus.INFO, "Get Banner message content");
		return UIHelper.getElementText(bannerMessageContainer);
	}

}
