package com.cloudleaf.automation.utils;

import java.io.IOException;

import com.cloudleaf.automation.base.BasePage;
import com.cloudleaf.automation.base.UIHelper;
import com.cloudleaf.automation.exceptions.CloudLeafException;

public class PageNavigations extends BasePage {

	public void navigateToAssetsPage() throws IOException, InterruptedException, CloudLeafException {

		dtHomePage.clickMenuHamburger();

		dtHomePage.clickManageAssetsOption();
		UIHelper.waitForElementNotDisplayed(manageAssetsPage.loaderIcon);
	}

	public void navigateToSensorsPage() throws IOException, InterruptedException, CloudLeafException {
		
		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		dtHomePage.clickMenuHamburger();

		dtHomePage.clickAdminOption();
		UIHelper.waitUntilElement(sensorsPage.addButton);
	}

	public void navigateToUsersPage() throws IOException, InterruptedException, CloudLeafException {

		dtHomePage.clickMenuHamburger();

		dtHomePage.clickUsersLink();
		UIHelper.waitForElementNotDisplayed(manageAssetsPage.loaderIcon);
	}

	public void navigateToMappingAdminPage() throws IOException, InterruptedException, CloudLeafException {

		dtHomePage.clickMenuHamburger();

		dtHomePage.clickMappingAdminOption();
		UIHelper.waitUntilElement(mappingAdminPage.addButton);

	}

}
