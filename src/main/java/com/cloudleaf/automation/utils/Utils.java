package com.cloudleaf.automation.utils;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.io.FileUtils;

import com.cloudleaf.automation.base.BasePage;
import com.cloudleaf.automation.exceptions.CloudLeafException;

public class Utils extends BasePage {

	static Properties properties = new Properties();
	public static final String REPORTS_DIR = System.getProperty("user.dir") + File.separator + "ExtentReports";
	public static final String SCREENSHOTS_DIR = System.getProperty("user.dir") + File.separator + "ScreenShots"
			+ File.separator;

	public static String createUUID() {

		return UUID.randomUUID().toString();
	}

	public static String generateRandomNumber() {
		return String.format("%06d", new Random().nextInt(1000000));
	}

	public static int generateRandomNumberInRange(int range) {

		return (int) ((Math.random() * (range - 0)) + 0);
	}

	public static void emptyFolder(File folder) {
		File[] files = folder.listFiles();
		if (files != null) { // some JVMs return null for empty dirs
			for (File f : files) {
				if (f.isDirectory()) {
//					deleteFolder(f);
				} else {
					f.delete();
				}
			}
		}
//		folder.delete();
//		System.out.println("Done...");
	}

	public static String calculateTimeDifference(String startDate, String endDate) throws Exception, ParseException {

		Date d1 = null;
		Date d2 = null;
		String timeDiff = "";

		d1 = sdf.parse(startDate);
		d2 = sdf.parse(endDate);

		// in milliseconds
		long diff = d2.getTime() - d1.getTime();

		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);

		if (diffDays != 0) {
			timeDiff = timeDiff + diffDays + " Day(s), ";
		}
		if (diffHours != 0) {
			timeDiff = timeDiff + diffHours + " Hour(s), ";
		}
		if (diffMinutes != 0) {
			timeDiff = timeDiff + diffMinutes + " Minute(s), ";
		}
		if (diffSeconds != 0) {
			timeDiff = timeDiff + diffSeconds + " Second(s).";
		}

		System.out.println(timeDiff);
		return timeDiff;
	}

	public static String getCurrentTimeStamp() {
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();

		return sdf.format(date);// returns date in dd-MM-yyyy HH:mm:ss
								// format eg:12-02-2012 12:34:42
	}

	public static String getCurrentTimeStamp(String format) {

		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();

		return sdf.format(date);// returns date in dd-MM-yyyy HH:mm:ss
								// format eg:12-02-2012 12:34:42
	}

	public static String addDaysToDate(String format, int noOfDaystoAdd) {

		SimpleDateFormat sdf = new SimpleDateFormat(format);

		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();

		cal.add(Calendar.DAY_OF_MONTH, 7);

		date = cal.getTime();

		return sdf.format(date);
	}

	public static void killChromeDriver() throws IOException {
		Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");
	}

	public static boolean compareTwoLists(ArrayList<String> firstList, ArrayList<String> secondList) {

		for (String title : secondList) {
			boolean found = false;
			for (String model : firstList) {
				if (title.contains(model)) {
					found = true;
					break;
				}
			}
			if (!found) {
				return false;
			}
		}
		return true;
	}

	public static void main(String args[]) {

		System.out.println(System.getProperty("java.io.tmpdir"));

	}

	public static String getFileSeparator() {
		String OS = System.getProperty("os.name");
		String fs = "";
		if (OS.contains("Windows")) {
			fs = "\\";
		} else if (OS.equalsIgnoreCase("MAC") || OS.equalsIgnoreCase("LINUX")) {
			fs = "/";
		}
		return fs;
	}

	public static String calculateTotalTimeTaken(ArrayList<String> startDateList, ArrayList<String> endDateList)
			throws CloudLeafException, ParseException {

		Date d1 = null;
		Date d2 = null;
		String timeDiff = "";
		long total = 0;

		System.out.println("startDateList.size(): " + startDateList.size());
		System.out.println("endDateList.size(): " + endDateList.size());
		if (startDateList.size() > 0) {
			for (int i = 0; i < startDateList.size(); i++) {
				d1 = sdf.parse(startDateList.get(i));
				d2 = sdf.parse(endDateList.get(i));
				long diff = d2.getTime() - d1.getTime();
				System.out.println(diff);
				total = total + diff;
			}
		}
		System.out.println("total: " + total);
		// in milliseconds

		// totalTimeTaken = totalTimeTaken + diff;
		long diffSeconds = total / 1000 % 60;
		long diffMinutes = total / (60 * 1000) % 60;
		long diffHours = total / (60 * 60 * 1000) % 24;
		long diffDays = total / (24 * 60 * 60 * 1000);

		if (diffDays != 0) {
			timeDiff = timeDiff + diffDays + " Day(s), ";
		}
		if (diffHours != 0) {
			timeDiff = timeDiff + diffHours + " Hour(s), ";
		}
		if (diffMinutes != 0) {
			timeDiff = timeDiff + diffMinutes + " Minute(s), ";
		}
		if (diffSeconds != 0) {
			timeDiff = timeDiff + diffSeconds + " Second(s).";
		}
		System.out.println(timeDiff);
		return timeDiff;
	}

	public static String getMessageBasedonTime() {

		String message = "";
		int currenTime = Integer.parseInt(DateTimeinMyFormat("HH"));
		if (currenTime < 12) {
			message = "Good Morning !";
		} else if (currenTime < 17) {
			message = "Good Afternoon !";
		} else {
			message = "Good Evening !";
		}
		return message;

	}

	public static String DateTimeinMyFormat(String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();

		return sdf.format(date);

	}
	
	public static void copyFile(File srcFile, File destFile) throws IOException {
		FileUtils.copyFile(srcFile, destFile);
		System.out.println("Done. file copied: " + srcFile);
	}


}
