package com.cloudleaf.webautomation;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.cloudleaf.automation.base.BasePage;
import com.cloudleaf.automation.base.UIHelper;
import com.cloudleaf.automation.exceptions.CloudLeafException;
import com.cloudleaf.automation.utils.Utils;

/**
 * 
 * @author snakka
 * @Date 07-June-2021
 */
public class TestSensorsPage extends BasePage {

	@Test
	public void C44651() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Verify user should land on Sensors page after clicking the Sensor link");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		Assert.assertTrue(driver.getCurrentUrl().contains("sensors"));

		Assert.assertTrue(sensorsPage.sensorsLink.isDisplayed());
	}

	@Test
	public void C44652() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest(
				"Verify that Sensor page should have Search text Field, Replace,Delete , Bind,Unbind buttons");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		Assert.assertTrue(commonPage.searchBar.isDisplayed(), "Search bar is not displayed in Sensors page");

		Assert.assertTrue(commonPage.bindButton.isDisplayed(), "Bind button is not displayed in Sensors page");

		Assert.assertTrue(commonPage.replaceButton.isDisplayed(), "Replace button is not displayed in Sensors page");

		Assert.assertTrue(commonPage.deleteButton.isDisplayed(), "Delete button not displayed in Sensors page");
	}

	@Test
	public void C44653() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest(
				"Verify that the Sensor table should contains Sensor ID, Asset Id, Type, Battery level and Overall status columns.");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		ArrayList<String> expectedColumnsList = new ArrayList<String>(
				Arrays.asList("Device ID", "Asset Id", "Type", "Battery Health", "Bind Status"));

		Assert.assertTrue(sensorsPage.getColumnHeadersList().containsAll(expectedColumnsList));

	}

	@Test
	public void C44654() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest(
				"Verify that user can select or unselect the columns in the table by clicking tick mark or X button.");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		commonPage.getCheckBoxesList().get(1).click();

		Assert.assertTrue(commonPage.bindButton.isEnabled());

		commonPage.getCheckBoxesList().get(1).click();

		Assert.assertFalse(commonPage.bindButton.isEnabled());

	}

	@Test
	public void C44655() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest(
				"Verify that Search returns the values depending on the Search term and display the results");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();
		sensorsPage.createSensor(sensorID);

		commonPage.enterSearchText(sensorID);

		Assert.assertEquals(commonPage.getColumnsList().get(0).getText().trim(), sensorID);

		sensorsList.add(sensorID);

	}

	@Test
	public void C44656() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Verify that Add Sensor should display the Sensor ID and Sensor Type dropdown");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		sensorsPage.clickAddButton();

		Assert.assertTrue(sensorsPage.sensorIDField.isDisplayed(), "Sensor ID field is not displayed");

		Assert.assertTrue(sensorsPage.sensorTypeDropdown.isDisplayed(), "Sensor type dropdown field is not displayed");

	}

	@Test
	public void C44657() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest(
				"Verify  user able to add a Sensor by passing ths Sensor Id and the selecting the Sensor Type and click on Save button");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();
		sensorsPage.createSensor(sensorID);

		commonPage.enterSearchText(sensorID);

		Assert.assertEquals(commonPage.getColumnsList().get(0).getText().trim(), sensorID);

	}

	@Test
	public void C44658() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Verify newly added sensor should display in the Sensor listing table.");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();
		sensorsPage.createSensor(sensorID);

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertTrue(UIHelper.getTextContentOfList(sensorsPage.sensorsTitlesList).contains(sensorID));

	}

	@Test
	public void C44659() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest(
				"Verify that user select the newly added Sensor and clicking on Bind button should open a Provision Sensor pop up message with Asset ID, Asset Category Drop Down with Cancel and Bind buttons");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();
		sensorsPage.createSensor(sensorID);

		commonPage.enterSearchText(sensorID);

		Assert.assertEquals(commonPage.getColumnsList().get(0).getText().trim(), sensorID);

		commonPage.getCheckBoxesList().get(1).click();

		Assert.assertTrue(commonPage.bindButton.isEnabled());

		commonPage.bindButton.click();

		Assert.assertTrue(sensorsPage.bindSensorPanel.isDisplayed(), "Bind Sensor Panel is not displayed");

		Assert.assertTrue(sensorsPage.bindSensorPanelHeader.isDisplayed(), "Bind Sensor Panel is not displayed");

		Assert.assertEquals(sensorsPage.bindSensorPanelHeader.getText().trim(), "Bind Sensor");

		Assert.assertTrue(sensorsPage.assetsSearchField.isDisplayed(), "Bind Sensor Panel is not displayed");

		Assert.assertTrue(sensorsPage.cancelButton.isDisplayed(), "Bind Sensor Panel is not displayed");

		Assert.assertTrue(sensorsPage.bindSensorPanelBindButton.isDisplayed(), "Bind Sensor Panel is not displayed");

	}

	@Test
	public void C44660() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Verify that user able to Bind the Sensor with the Asset ID succesfully");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		pageNavigations.navigateToSensorsPage();

		commonPage.enterSearchText(sensorID);

		Assert.assertEquals(commonPage.getColumnsList().get(0).getText().trim(), sensorID);

		commonPage.getCheckBoxesList().get(1).click();

		Thread.sleep(2000);

		Assert.assertTrue(commonPage.bindButton.isEnabled());

		commonPage.bindButton.click();

		UIHelper.enterCharByChar(sensorsPage.assetsSearchField, assetID);

		UIHelper.clickElement(commonPage.searchResult);

		sensorsPage.bindSensorPanelBindButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		commonPage.enterSearchText(sensorID);

		Assert.assertEquals(commonPage.getColumnsList().get(4).getText().trim(), "Bound");
	}

	@Test
	public void C44661() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest(
				"Verify user gets  \" OK to Unbind selected sensors? \" Popup with Cancel and OK button when user selected a binded sensor and click on Unbind");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		pageNavigations.navigateToSensorsPage();

		commonPage.enterSearchText(sensorID);

		Assert.assertEquals(commonPage.getColumnsList().get(0).getText().trim(), sensorID);

		commonPage.getCheckBoxesList().get(1).click();

		Thread.sleep(2000);

		Assert.assertTrue(commonPage.bindButton.isEnabled());

		commonPage.bindButton.click();

		UIHelper.enterCharByChar(sensorsPage.assetsSearchField, assetID);

		UIHelper.clickElement(commonPage.searchResult);

		sensorsPage.bindSensorPanelBindButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		commonPage.enterSearchText(sensorID);

		Assert.assertEquals(commonPage.getColumnsList().get(4).getText().trim(), "Bound");

		commonPage.getCheckBoxesList().get(1).click();

		sensorsPage.unbindButton.click();

		Assert.assertEquals(commonPage.confirmDialogMessage.getText().trim(), "Are you sure to unbind sensor(s)?");

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

	}

	@Test
	public void C44662() throws IOException, InterruptedException, CloudLeafException {
		test = extent
				.startTest("Verify user able to Unbind the sensor after clicking the OK button in the popup message.");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		pageNavigations.navigateToSensorsPage();

		commonPage.enterSearchText(sensorID);

		Assert.assertEquals(commonPage.getColumnsList().get(0).getText().trim(), sensorID);

		commonPage.getCheckBoxesList().get(1).click();

		Thread.sleep(2000);

		Assert.assertTrue(commonPage.bindButton.isEnabled());

		commonPage.bindButton.click();

		UIHelper.enterCharByChar(sensorsPage.assetsSearchField, assetID);

		UIHelper.clickElement(commonPage.searchResult);

		sensorsPage.bindSensorPanelBindButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		commonPage.enterSearchText(sensorID);

		Assert.assertEquals(commonPage.getColumnsList().get(4).getText().trim(), "Bound");

		commonPage.getCheckBoxesList().get(1).click();

		sensorsPage.unbindButton.click();

		Assert.assertEquals(commonPage.confirmDialogMessage.getText().trim(), "Are you sure to unbind sensor(s)?");

		commonPage.confirmWindowYesButton.click();

		UIHelper.waitUntilElement(commonPage.bannerMessageContainer);

		Assert.assertEquals(commonPage.bannerMessageContainer.getText().trim(), "Sensor unbound\n" + sensorID);

	}

	@Test
	public void C44663() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest(
				"Verify the user able to Delete the unbinded sensors successfully by clicking the OK button");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		pageNavigations.navigateToSensorsPage();

		commonPage.enterSearchText(sensorID);

		Assert.assertEquals(commonPage.getColumnsList().get(0).getText().trim(), sensorID);

		commonPage.getCheckBoxesList().get(1).click();

		Thread.sleep(2000);

		Assert.assertTrue(commonPage.bindButton.isEnabled());

		commonPage.bindButton.click();

		UIHelper.enterCharByChar(sensorsPage.assetsSearchField, assetID);

		UIHelper.clickElement(commonPage.searchResult);

		sensorsPage.bindSensorPanelBindButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		commonPage.enterSearchText(sensorID);

		Assert.assertEquals(commonPage.getColumnsList().get(4).getText().trim(), "Bound");

		commonPage.getCheckBoxesList().get(1).click();

		sensorsPage.unbindButton.click();

		Assert.assertEquals(commonPage.confirmDialogMessage.getText().trim(), "Are you sure to unbind sensor(s)?");

		commonPage.confirmWindowYesButton.click();

		UIHelper.waitUntilElement(commonPage.bannerMessageContainer);

		Assert.assertEquals(commonPage.bannerMessageContainer.getText().trim(), "Sensor unbound\n" + sensorID);

		commonPage.deleteButton.click();

		commonPage.confirmWindowYesButton.click();

//		Assert.assertEquals(commonPage.bannerMessageContainer.getText().trim(), "Sensor deleted\n" + sensorID);

	}

	@Test
	public void C44664() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Verify user can delete mutilple sensors by selecting multiple  unbinded sensors");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();
		String sensorID;

		for (int i = 0; i < 3; i++) {
			sensorID = "Sensor" + Utils.generateRandomNumber();

			sensorsPage.createSensor(sensorID);
			sensorsList.add(sensorID);
		}

		sensorsPage.unBoundCheckbox.click();

		commonPage.waitUntilLoaderIconNotVisible();

		commonPage.selectMultipleCheckboxes(sensorsList);

		commonPage.deleteButton.click();

		commonPage.confirmWindowYesButton.click();

		UIHelper.waitUntilElement(commonPage.popupMessageContainer);

		Assert.assertTrue(commonPage.popupMessageContainer.isDisplayed());

		Assert.assertTrue(commonPage.getPopMessageAssetsListContent().containsAll(sensorsList));

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

	}

	@Test
	public void C44669() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest(
				"Verify user gets a pop up in the bottom when he tried to Delete a binded asset . \"Please Unbind before Deleting\"");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		pageNavigations.navigateToSensorsPage();

		commonPage.enterSearchText(sensorID);

		Assert.assertEquals(commonPage.getColumnsList().get(0).getText().trim(), sensorID);

		commonPage.getCheckBoxesList().get(1).click();

		Thread.sleep(2000);

		Assert.assertTrue(commonPage.bindButton.isEnabled());

		commonPage.bindButton.click();

		UIHelper.enterCharByChar(sensorsPage.assetsSearchField, assetID);

		UIHelper.clickElement(commonPage.searchResult);

		sensorsPage.bindSensorPanelBindButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		commonPage.enterSearchText(sensorID);

		Assert.assertEquals(commonPage.getColumnsList().get(4).getText().trim(), "Bound");

		commonPage.getCheckBoxesList().get(1).click();

		commonPage.deleteButton.click();

		commonPage.confirmWindowYesButton.click();

		UIHelper.waitUntilElementClickable(commonPage.bannerMessageContainer);

		Assert.assertEquals(commonPage.bannerMessageContainer.getText().trim(),
				"Please unbind sensor before Deleting\n" + sensorID);

	}

	@Test
	public void C44670() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest(
				"Verify that Battery health is displayed properly in the Sensor listing page as per the battery level");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		Assert.assertTrue(sensorsPage.getColumnHeadersList().contains("Battery Health"));

	}

	@Test
	public void C44675() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate Sensor Type filter in Sensor Inventory page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		Assert.assertTrue(sensorsPage.getSensorTypeCheckboxesList().size() > 0, "Type filter is not displayed");
	}

	@Test
	public void C44677() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate Sensor Type filter with biotempak selected in Sensor Inventory page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		Assert.assertTrue(sensorsPage.getSensorTypeCheckboxesList().size() > 0, "Type filter is not displayed");

		sensorsPage.getSensorTypeCheckboxesList().get(0).click();

		commonPage.waitUntilLoaderIconNotVisible();

		System.out.println(sensorsPage.getSensorTypeCheckboxesList().get(0).getText().trim());

		System.out.println(commonPage.getColumnsListData().get(2));

		Assert.assertEquals(commonPage.getColumnsListData().get(2),
				sensorsPage.getSensorTypeCheckboxesList().get(0).getText().trim());

	}

	@Test
	public void C44678() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate Sensor Type filter with biotempak bt3-4m selected in Sensor Inventory page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		Assert.assertTrue(sensorsPage.getSensorTypeCheckboxesList().size() > 0, "Type filter is not displayed");

		sensorsPage.getSensorTypeCheckboxesList().get(1).click();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getColumnsListData().get(2),
				sensorsPage.getSensorTypeCheckboxesList().get(1).getText().trim());

		sensorsPage.getSensorTypeCheckboxesList().get(1).click();

		commonPage.waitUntilLoaderIconNotVisible();

	}

	@Test
	public void C44679() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate Sensor Type filter with biotempak t70 selected in Sensor Inventory page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		Assert.assertTrue(sensorsPage.getSensorTypeCheckboxesList().size() > 0, "Type filter is not displayed");

		sensorsPage.getSensorTypeCheckboxesList().get(2).click();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getColumnsListData().get(2),
				sensorsPage.getSensorTypeCheckboxesList().get(2).getText().trim());

		sensorsPage.getSensorTypeCheckboxesList().get(2).click();

		commonPage.waitUntilLoaderIconNotVisible();

	}

	@Test
	public void C44680() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate Sensor Type filter with biotempak t72 selected in Sensor Inventory page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		Assert.assertTrue(sensorsPage.getSensorTypeCheckboxesList().size() > 0, "Type filter is not displayed");

		sensorsPage.getSensorTypeCheckboxesList().get(3).click();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getColumnsListData().get(2),
				sensorsPage.getSensorTypeCheckboxesList().get(3).getText().trim());

		sensorsPage.getSensorTypeCheckboxesList().get(3).click();

		commonPage.waitUntilLoaderIconNotVisible();

	}

	@Test
	public void C60271() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate Sensor Type filter with nlk-at4  selected in Sensor Inventory page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		Assert.assertTrue(sensorsPage.getSensorTypeCheckboxesList().size() > 0, "Type filter is not displayed");

		sensorsPage.getSensorTypeCheckboxesList().get(4).click();
		Thread.sleep(3000);

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getColumnsListData().get(2),
				sensorsPage.getSensorTypeCheckboxesList().get(4).getText().trim().toUpperCase());

		sensorsPage.getSensorTypeCheckboxesList().get(4).click();
		Thread.sleep(3000);

		commonPage.waitUntilLoaderIconNotVisible();

	}

	@Test
	public void C60272() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate Sensor Type filter with nlk-at4  selected in Sensor Inventory page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		Assert.assertTrue(sensorsPage.getSensorTypeCheckboxesList().size() > 0, "Type filter is not displayed");

		sensorsPage.getSensorTypeCheckboxesList().get(5).click();
		Thread.sleep(3000);

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getColumnsListData().get(2),
				sensorsPage.getSensorTypeCheckboxesList().get(5).getText().trim().toUpperCase());

		sensorsPage.getSensorTypeCheckboxesList().get(5).click();
		Thread.sleep(3000);

		commonPage.waitUntilLoaderIconNotVisible();

	}

	@Test
	public void C44681() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate sensor table data when user changes the filter types in Status filter");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		sensorsPage.unBoundCheckbox.click();
		Thread.sleep(3000);

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getColumnsListData().get(4), "Unbound");

		sensorsPage.unBoundCheckbox.click();

		commonPage.waitUntilLoaderIconNotVisible();

		sensorsPage.boundCheckbox.click();
		Thread.sleep(3000);

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getColumnsListData().get(4), "Bound");

		sensorsPage.boundCheckbox.click();

		commonPage.waitUntilLoaderIconNotVisible();

	}

	@Test
	public void C44683() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate sensor table data when user changes the filter types in Sensor Type filter");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		Assert.assertTrue(sensorsPage.getSensorTypeCheckboxesList().size() > 0, "Type filter is not displayed");

		for (int i = 0; i < sensorsPage.getSensorTypeCheckboxesList().size(); i++) {
			sensorsPage.getSensorTypeCheckboxesList().get(i).click();
			Thread.sleep(3000);

			commonPage.waitUntilLoaderIconNotVisible();

			Assert.assertTrue(commonPage.getColumnsListData().get(2)
					.equalsIgnoreCase(sensorsPage.getSensorTypeCheckboxesList().get(i).getText().trim()));

			sensorsPage.getSensorTypeCheckboxesList().get(i).click();
			Thread.sleep(3000);

			commonPage.waitUntilLoaderIconNotVisible();
		}

	}

	@Test()
	public void C44684() throws ParseException, Exception {
		test = extent.startTest("Validate the Sensor page loading time for large number of Sensor data");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		String beforeLoadTime = Utils.getCurrentTimeStamp();

		pageNavigations.navigateToSensorsPage();

		String afterLoadTime = Utils.getCurrentTimeStamp();

		String timeElapsed = Utils.calculateTimeDifference(beforeLoadTime, afterLoadTime);

		System.out.println("Time taken for page load: " + timeElapsed);
	}

	@Test()
	public void C44685() throws ParseException, Exception {
		test = extent.startTest("Validate Replace button in Sensors page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		Assert.assertTrue(commonPage.replaceButton.isDisplayed(), "Replace button is not displayed in Sensors page");

	}

	@Test()
	public void C44686() throws ParseException, Exception {
		test = extent.startTest("Validate Replace button is disabled when user selects an unbinded sensor in Sensors page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		Assert.assertTrue(commonPage.replaceButton.isDisplayed(), "Replace button is not displayed in Sensors page");

		sensorsPage.unBoundCheckbox.click();
		Thread.sleep(3000);

		commonPage.getCheckBoxesList().get(1).click();

		Assert.assertFalse(commonPage.replaceButton.isEnabled(), "Replace button is not disabled");

	}

	@Test
	public void C44687() throws IOException, InterruptedException, CloudLeafException {
		
		test = extent.startTest("Validate Replace button is disabled when user selects multiple sensors in Sensors page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();
		String sensorID;

		for (int i = 0; i < 3; i++) {
			sensorID = "Sensor" + Utils.generateRandomNumber();

			sensorsPage.createSensor(sensorID);
			sensorsList.add(sensorID);
		}

		commonPage.selectMultipleCheckboxes(sensorsList);

		Assert.assertFalse(commonPage.replaceButton.isEnabled(), "Replace button is not disabled");

	}

}
