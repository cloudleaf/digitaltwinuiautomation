package com.cloudleaf.webautomation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.validator.GenericValidator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.cloudleaf.automation.base.BasePage;
import com.cloudleaf.automation.base.UIHelper;
import com.cloudleaf.automation.exceptions.CloudLeafException;
import com.cloudleaf.automation.utils.Utils;


/**
 * 
 * @author snakka
 * @Date 26-May-2021
 *
 */
public class TestAssetsPage extends BasePage {

	@Test
	public void C44707() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Verify the fields displayed after selecting Manage Assets link");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		Assert.assertTrue(manageAssetsPage.isSearchBarDisplayed(), "Expected Search bar is not displayed");

		Assert.assertTrue(manageAssetsPage.isUnbindButtonDisplayed(), "Expected Unbind button is not displayed");

		Assert.assertTrue(manageAssetsPage.isDeleteButtonDisplayed(), "Expected Delete button is not displayed");

		Assert.assertTrue(manageAssetsPage.isBulkBindButtonDisplayed(), "Expected Bulk bind is not displayed");

		Assert.assertTrue(manageAssetsPage.isBulkUnbindButtonDisplayed(), "Expected Bulk unbind is not displayed");

		Assert.assertTrue(manageAssetsPage.isAssetCategorySectionDisplayed(),
				"Expected Asset Category section is not displayed");

		Assert.assertTrue(manageAssetsPage.isFilterBySectionDisplayed(), "Expected Filter by section is not displayed");

		Assert.assertTrue(manageAssetsPage.AreListOfAssetsDisplayed(), "Expected List of Assets are not displayed");

	}

	@Test
	public void C44708() throws IOException, InterruptedException, CloudLeafException {
		test = extent
				.startTest("Verify user can add new asset by providing the Asset Id*,Asset name and Asset Category");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		assetsList.add(assetID);

	}

	@Test
	public void C44709() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Verify Adding an asset without giving asset name");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(0).getText().trim(), assetID);

	}

	@Test
	public void C44710() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Add an asset by entering data in all fields");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);
		String mfgName = "MFG" + Utils.generateRandomNumber();
		String warehouseLocation = "WHL" + Utils.generateRandomNumber();
		String maintenanceDate = Utils.addDaysToDate(MAINTENANCE_DATE_FORMAT, 7);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost, mfgName, warehouseLocation,
				maintenanceDate);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(0).getText().trim(), assetID);

	}

	@Test
	public void C44711() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Add an asset by entering data only in mandatory fields");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;

		manageAssetsPage.createAsset(assetID, "", assetSKUCode, 0);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

	}

	@Test
	public void C44712() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Open asset detail page of bound asset by selecting asset name");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		if (manageAssetsPage.bindButton.isEnabled()) {
			manageAssetsPage.bindButton.click();
		}

		manageAssetsPage.bindAssetWithSensor(sensorID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "MONITORED");

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.clickAssetDetailsLink();

	}

	@Test
	public void C44713() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Open asset detail page of unbound asset by selecting asset name");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		Assert.assertEquals(manageAssetsPage.getAssetSummaryList().get(0).getText().trim(), assetID);

		Assert.assertEquals(manageAssetsPage.getAssetSummaryList().get(2).getText().trim(), assetSKUCode);

		Assert.assertEquals(manageAssetsPage.getAssetCustodyList().get(0).getText().trim(), "$" + assetCost);

		Assert.assertEquals(manageAssetsPage.getAssetCustodyList().get(1).getText().trim(), "Good");

	}

	@Test
	public void C44714() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Verify user can delete the existing Asset");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
				manageAssetsPage.statusFilterLabel);

		WebElement checkbox = manageAssetsPage.getFilterCheckboxesList().get(1);
		UIHelper.waitUntilElementClickable(manageAssetsPage.loadNext50ItemsLink);

		Actions actions = new Actions(driver);
		actions.moveToElement(checkbox);
		checkbox.click();
		commonPage.waitUntilLoaderIconNotVisible();
		checkbox = manageAssetsPage.getFilterCheckboxesList().get(2);

		actions = new Actions(driver);
		actions.moveToElement(checkbox);
		checkbox.click();
		commonPage.waitUntilLoaderIconNotVisible();
		Assert.assertTrue(manageAssetsPage.getAssetsStatusList().contains("UNMONITORED"));

		manageAssetsPage.getCheckBoxesList().get(1).click();

		if (commonPage.deleteButton.isEnabled()) {
			commonPage.deleteButton.click();
			commonPage.clickConfirmWindowYesButton();
		}

		Assert.assertTrue(commonPage.getBannerMessageContent().contains("Asset Deleted"));

	}

	@Test
	public void C44715() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Verify if delete button is disabled when bound asset is selected");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		if (manageAssetsPage.bindButton.isEnabled()) {
			manageAssetsPage.bindButton.click();
		}

		manageAssetsPage.bindAssetWithSensor(sensorID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "MONITORED");

		manageAssetsPage.getCheckBoxesList().get(1).click();

		if (commonPage.deleteButton.isEnabled()) {
			commonPage.deleteButton.click();
			commonPage.clickConfirmWindowYesButton();
		}

		Assert.assertEquals(commonPage.getBannerMessageContent(),
				"Asset don't exist or its monitored" + "\n" + assetID);

	}

	@Test
	public void C44716() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest(
				"Verify user able to delete multiple assets by selecting the checkboxes and click on Delete Selected");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String assetID;
		String qrCode;
		String assetSKUCode;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		for (int i = 0; i < 3; i++) {
			assetID = "Asset" + Utils.generateRandomNumber();
			qrCode = "AssetQR" + Utils.generateRandomNumber();
			assetSKUCode = "assetSKU" + Utils.generateRandomNumber();
			manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);
			pageNavigations.navigateToAssetsPage();
			manageAssetsPage.enterSearchText(assetID);
			Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);
			UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);
			driver.navigate().refresh();

			commonPage.waitUntilLoaderIconNotVisible();
		}

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		commonPage.selectMultipleCheckboxes(assetsList);

		if (commonPage.deleteButton.isEnabled()) {
			commonPage.deleteButton.click();
			commonPage.clickConfirmWindowYesButton();
		}

		UIHelper.waitUntilElement(commonPage.popupMessageContainer);

		Assert.assertTrue(commonPage.getPopMessageAssetsListContent().containsAll(assetsList));

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

	}

	@Test
	public void C44717() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Verify user is able to bind asset by providing the Sensor ID");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		if (manageAssetsPage.bindButton.isEnabled()) {
			manageAssetsPage.bindButton.click();
		}

		manageAssetsPage.bindAssetWithSensor(sensorID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "MONITORED");

	}

	@Test
	public void C44718() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Verify user is able to Unbind asset which is bound");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		if (manageAssetsPage.bindButton.isEnabled()) {
			manageAssetsPage.bindButton.click();
		}

		manageAssetsPage.bindAssetWithSensor(sensorID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "MONITORED");

		manageAssetsPage.getCheckBoxesList().get(1).click();

		driver.findElements(manageAssetsPage.unbindButtonsList).get(0).click();

		commonPage.clickConfirmWindowYesButton();

		manageAssetsPage.getCheckBoxesList().get(1).click();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "UNMONITORED");

	}

	@Test
	public void C44720() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Verify user able to create a new Asset Category");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.clickAssetCategoryAddButton();

		String assetCategory = "assetCat" + Utils.generateRandomNumber();

		manageAssetsPage.enterAssetCategoryName(assetCategory);

		manageAssetsPage.clickSaveButton();

		Assert.assertTrue(commonPage.getBannerMessageContent().contains(assetCategory));

		Assert.assertTrue(manageAssetsPage.getassetCateogyListContent().contains(assetCategory));

		manageAssetsPage.deleteAssetCategory(assetCategory);

		Assert.assertTrue(commonPage.getBannerMessageContent().contains(assetCategory));
	}

	@Test
	public void C44721() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Verify user able to delete an existing Asset category ( Category should be empty)");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.clickAssetCategoryAddButton();

		String assetCategory = "assetCat" + Utils.generateRandomNumber();

		manageAssetsPage.enterAssetCategoryName(assetCategory);

		manageAssetsPage.clickSaveButton();

		Assert.assertTrue(commonPage.getBannerMessageContent().contains(assetCategory));

		Assert.assertTrue(manageAssetsPage.getassetCateogyListContent().contains(assetCategory));

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		Thread.sleep(2000);

		WebElement checkbox = driver
				.findElement(By.xpath("//span[text()='" + assetCategory + "']/preceding::input[1]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", checkbox);
		Actions actions = new Actions(driver);
		actions.moveToElement(checkbox).click().build().perform();
		UIHelper.clickElement(manageAssetsPage.assetCategoryDeleteIcon);

		Assert.assertEquals(manageAssetsPage.getConfirmWindowMessage(), "OK to delete the category: " + assetCategory);

		commonPage.clickConfirmWindowYesButton();
	}

	@Test
	public void C44722() throws IOException, InterruptedException, CloudLeafException {

		test = extent
				.startTest("Verify that proper error message is displayed when we tried to remove an Asset Category");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.clickAssetCategoryAddButton();

		String assetCategory = "assetCat" + Utils.generateRandomNumber();

		manageAssetsPage.enterAssetCategoryName(assetCategory);

		manageAssetsPage.clickSaveButton();

		Assert.assertTrue(commonPage.getBannerMessageContent().contains(assetCategory));

		Assert.assertTrue(manageAssetsPage.getassetCateogyListContent().contains(assetCategory));

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCategory, assetCost);

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		Thread.sleep(2000);

		WebElement checkbox = driver
				.findElement(By.xpath("//span[text()='" + assetCategory + "']/preceding::input[1]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", checkbox);
		Actions actions = new Actions(driver);
		actions.moveToElement(checkbox).click().build().perform();
		UIHelper.clickElement(manageAssetsPage.assetCategoryDeleteIcon);

		Assert.assertTrue(
				commonPage.getBannerMessageContent().contains("Delete all assets before deleting category"));
	}

	@Test
	public void C44723() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Filter assets based on status ( All , Bound, Unbound)");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
				manageAssetsPage.statusFilterLabel);

		if (manageAssetsPage.getFilterCheckboxesList().size() > 0) {

			WebElement checkbox = manageAssetsPage.getFilterCheckboxesList().get(0);

			Actions actions = new Actions(driver);
			actions.moveToElement(checkbox);
			checkbox.click();
			commonPage.waitUntilLoaderIconNotVisible();
			Assert.assertTrue(manageAssetsPage.getAssetsStatusList().contains("MONITORED"));
			checkbox.click();
			checkbox = manageAssetsPage.getFilterCheckboxesList().get(1);

			actions = new Actions(driver);
			actions.moveToElement(checkbox);
			checkbox.click();
			commonPage.waitUntilLoaderIconNotVisible();
			Assert.assertTrue(manageAssetsPage.getAssetsStatusList().contains("UNMONITORED"));
			checkbox.click();
		}

	}

	@Test
	public void C44724() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Filter assets based on state ( In Possession, Available, In Transfer))");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
				manageAssetsPage.statusFilterLabel);

		if (manageAssetsPage.getFilterCheckboxesList().size() > 0) {

			WebElement checkbox = manageAssetsPage.getFilterCheckboxesList().get(2);

			Actions actions = new Actions(driver);
			actions.moveToElement(checkbox);
			checkbox.click();
			commonPage.waitUntilLoaderIconNotVisible();
			Assert.assertTrue(manageAssetsPage.getAssetsStateList().contains("AVAILABLE"));
			checkbox.click();
			checkbox = manageAssetsPage.getFilterCheckboxesList().get(3);

			actions = new Actions(driver);
			actions.moveToElement(checkbox);
			checkbox.click();
			commonPage.waitUntilLoaderIconNotVisible();
			Assert.assertTrue(manageAssetsPage.getAssetsStateList().contains("IN_POSSESSION"));
			checkbox.click();

			checkbox = manageAssetsPage.getFilterCheckboxesList().get(4);

			actions = new Actions(driver);
			actions.moveToElement(checkbox);
			checkbox.click();
			commonPage.waitUntilLoaderIconNotVisible();
			Assert.assertTrue(manageAssetsPage.getAssetsStateList().contains("IN_TRANSFER"));
		}

	}

	@Test
	public void C44725() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Filter assets based on category");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.clickAssetCategoryAddButton();

		String assetCategory = "assetCat" + Utils.generateRandomNumber();

		manageAssetsPage.enterAssetCategoryName(assetCategory);

		manageAssetsPage.clickSaveButton();

		Assert.assertTrue(commonPage.getBannerMessageContent().contains(assetCategory));

		Assert.assertTrue(manageAssetsPage.getassetCateogyListContent().contains(assetCategory));

		manageAssetsPage.selectAssetCategoryInLeftPane(assetCategory);

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertTrue(manageAssetsPage.getAssetCategoryLabelList().contains(assetCategory));

	}

	@Test
	public void C44727() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Search for assets in Search field");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

	}

	@Test
	public void C44728() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Unbind all assets in Manage Assets list");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = null;
		ArrayList<String> sensorIDSList = new ArrayList<String>();
		for (int i = 0; i < 3; i++) {
			sensorID = "Sensor" + Utils.generateRandomNumber();
			sensorsPage.createSensor(sensorID);
//			Assert.assertTrue(manageAssetsPage.getBannerMessageContent().contains("Sensor created"));
			sensorIDSList.add(sensorID);
		}

		pageNavigations.navigateToAssetsPage();

		String assetID;
		String qrCode;
		String assetSKUCode;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		for (int i = 0; i < 3; i++) {
			assetID = "Asset" + Utils.generateRandomNumber();
			assetsList.add(assetID);
			qrCode = "AssetQR" + Utils.generateRandomNumber();
			assetSKUCode = "assetSKU" + Utils.generateRandomNumber();
			manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);
			pageNavigations.navigateToAssetsPage();
			manageAssetsPage.enterSearchText(assetID);
			Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);
			UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);
			commonPage.waitUntilLoaderIconNotVisible();
		}

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		for (int i = 0; i < assetsList.size(); i++) {

			manageAssetsPage.enterSearchText(assetsList.get(i));

			Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetsList.get(i));

			manageAssetsPage.getCheckBoxesList().get(1).click();

			if (manageAssetsPage.bindButton.isEnabled()) {
				manageAssetsPage.bindButton.click();
			}

			manageAssetsPage.bindAssetWithSensor(sensorIDSList.get(i));

			manageAssetsPage.getCheckBoxesList().get(1).click();

			UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);
			commonPage.waitUntilLoaderIconNotVisible();

			driver.navigate().refresh();

			commonPage.waitUntilLoaderIconNotVisible();

			manageAssetsPage.enterSearchText(assetsList.get(i));

			Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetsList.get(i));

			Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "MONITORED");
		}

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		for (int i = 0; i < assetsList.size(); i++) {

			manageAssetsPage.enterSearchText(assetsList.get(i));

			Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetsList.get(i));

			manageAssetsPage.getCheckBoxesList().get(1).click();

			if (driver.findElements(manageAssetsPage.unbindButtonsList).get(0).isEnabled()) {
				driver.findElements(manageAssetsPage.unbindButtonsList).get(0).click();
				commonPage.clickConfirmWindowYesButton();
			}
			Thread.sleep(2000);
			manageAssetsPage.getCheckBoxesList().get(1).click();
			Thread.sleep(2000);
			manageAssetsPage.enterSearchText(assetsList.get(i));
			Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "UNMONITORED");
			UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);
			commonPage.waitUntilLoaderIconNotVisible();
		}

	}

	@Test
	public void C44736() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Validate State column in the Manage Assets list");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		ArrayList<String> expectedAssetStates = new ArrayList<String>(
				Arrays.asList("AVAILABLE", "IN_POSSESSION", "IN_TRANSFER"));

		Assert.assertTrue(Utils.compareTwoLists(manageAssetsPage.getAssetsStateList(), expectedAssetStates));

	}

	@Test
	public void C44745() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Count of assets should  change based on selection of Asset category from filter");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.clickAssetCategoryAddButton();

		String assetCategory = "assetCat" + Utils.generateRandomNumber();

		manageAssetsPage.enterAssetCategoryName(assetCategory);

		manageAssetsPage.clickSaveButton();

		Assert.assertTrue(commonPage.getBannerMessageContent().contains(assetCategory));

		Assert.assertTrue(manageAssetsPage.getassetCateogyListContent().contains(assetCategory));

		for (int i = 0; i < 3; i++) {
			String uniqueID = Utils.generateRandomNumber();

			String assetID = "Asset" + uniqueID;
			String qrCode = "AssetQR" + uniqueID;
			String assetSKUCode = "assetSKU" + uniqueID;
			int assetCost = Utils.generateRandomNumberInRange(9999);

			manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCategory, assetCost);
		}

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		Thread.sleep(2000);

		WebElement checkbox = driver
				.findElement(By.xpath("//span[text()='" + assetCategory + "']/preceding::input[1]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", checkbox);
		Actions actions = new Actions(driver);
		actions.moveToElement(checkbox).click().build().perform();

		UIHelper.waitUntilElement(manageAssetsPage.assetDisplayCountHeader);

		int assetCategoryCount = Integer
				.parseInt(driver.findElement(By.xpath("//span[text()='" + assetCategory + "']/following::span[1]"))
						.getText().replace(")", "").replace("(", ""));

		Assert.assertEquals(manageAssetsPage.getAssetsRowCount(), assetCategoryCount);
	}

	@Test
	public void C44746() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Validate Asset created date for new Assets");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(2).getText().trim().split(",")[0],
				Utils.getCurrentTimeStamp("M/d/yy"));

	}

	@Test
	public void C44747() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Validate Asset created date for existing Assets");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost);

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(2).getText().trim().split(",")[0],
				Utils.getCurrentTimeStamp("M/d/yy"));

		String DATE_FORMAT_NOW = "M/d/yy, hh:mm a";

		Assert.assertTrue(GenericValidator.isDate(commonPage.getColumnsList().get(2).getText().trim(),
				DATE_FORMAT_NOW, true));

	}

	@Test
	public void C44748() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Verify all fields in Asset Summary pane of asset");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);
		String mfgName = "MFG" + Utils.generateRandomNumber();
		String warehouseLocation = "WHL" + Utils.generateRandomNumber();
		String maintenanceDate = Utils.addDaysToDate(MAINTENANCE_DATE_FORMAT, 7);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost, mfgName, warehouseLocation,
				maintenanceDate);

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		String dateAdded = commonPage.getColumnsList().get(2).getText().trim();

		ArrayList<String> assetsDetailsList = new ArrayList<String>(
				Arrays.asList(assetID, "Unmonitored", assetSKUCode, qrCode, "Available", dateAdded, dateAdded));

		ArrayList<String> assetsCustodyList = new ArrayList<String>(
				Arrays.asList("$100", "Good", mfgName, warehouseLocation));

		Assert.assertTrue(manageAssetsPage.getAssetsDetailsList().equals(assetsDetailsList));

		Assert.assertTrue(manageAssetsPage.getAssetsCustodyDetailsListList().containsAll(assetsCustodyList));

	}

	@Test
	public void C44749() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Verify all buttons in Asset Summary pane of asset");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);
		String mfgName = "MFG" + Utils.generateRandomNumber();
		String warehouseLocation = "WHL" + Utils.generateRandomNumber();
		String maintenanceDate = Utils.addDaysToDate(MAINTENANCE_DATE_FORMAT, 7);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost, mfgName, warehouseLocation,
				maintenanceDate);

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.clickAssetDetailsLink();

		ArrayList<String> checkoutButtonsList = new ArrayList<String>(Arrays.asList("bind", "edit", "check out"));

		Assert.assertTrue(manageAssetsPage.getCheckoutButtonsList().containsAll(checkoutButtonsList));

	}

	@Test
	public void C44750() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Verify if checkout button is enabled for a newly added asset");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);
		String mfgName = "MFG" + Utils.generateRandomNumber();
		String warehouseLocation = "WHL" + Utils.generateRandomNumber();
		String maintenanceDate = Utils.addDaysToDate(MAINTENANCE_DATE_FORMAT, 7);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost, mfgName, warehouseLocation,
				maintenanceDate);

		driver.navigate().refresh();

		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		Assert.assertTrue(manageAssetsPage.isCheckoutButtonDisplayedAndEnabled());

	}

	@Test
	public void C44751() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Delete an asset which has history of custody");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
				manageAssetsPage.statusFilterLabel);

		WebElement checkbox = manageAssetsPage.getFilterCheckboxesList().get(3);

		Actions actions = new Actions(driver);
		actions.moveToElement(checkbox);
		checkbox.click();
		Thread.sleep(2000);
		Assert.assertTrue(manageAssetsPage.getAssetsStateList().contains("IN_POSSESSION"));

		manageAssetsPage.getCheckBoxesList().get(1).click();
		commonPage.deleteButton.click();
		commonPage.clickConfirmWindowYesButton();

		Assert.assertTrue(commonPage.getBannerMessageContent().contains("Unable to delete asset"));

	}

}
