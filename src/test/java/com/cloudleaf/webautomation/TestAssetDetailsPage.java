package com.cloudleaf.webautomation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.cloudleaf.automation.base.BasePage;
import com.cloudleaf.automation.base.UIHelper;
import com.cloudleaf.automation.exceptions.CloudLeafException;
import com.cloudleaf.automation.utils.PropertyFileUtils;
import com.cloudleaf.automation.utils.RestAPIUtility;
import com.cloudleaf.automation.utils.Utils;

public class TestAssetDetailsPage extends BasePage {

	@Test
	public void C44795() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check for fields in Bound Asset details page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);
		String mfgName = "MFG" + Utils.generateRandomNumber();
		String warehouseLocation = "WHL" + Utils.generateRandomNumber();
		String maintenanceDate = Utils.addDaysToDate(MAINTENANCE_DATE_FORMAT, 7);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost, mfgName, warehouseLocation,
				maintenanceDate);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		if (manageAssetsPage.bindButton.isEnabled()) {
			manageAssetsPage.bindButton.click();
		}

		manageAssetsPage.bindAssetWithSensor(sensorID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "MONITORED");

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.clickAssetDetailsLink();
		commonPage.waitUntilLoaderIconNotVisible();
		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);

		ArrayList<String> assetSummaryLabelsList = new ArrayList<String>(Arrays.asList("Asset ID:", "Category:",
				"Status:", "SKU:", "QR Code:", "Sensor ID:", "State:", "Modified on:", "Created on:"));

		ArrayList<String> assetCustodyLabelsList = new ArrayList<String>(
				Arrays.asList("Cost:", "Condition:", "Manufacturer:", "Warehouse Location:", "Maintenance Date:"));
		
		Assert.assertEquals(UIHelper.getTextContentOfList(assetDetailsPage.assetSummaryList), assetSummaryLabelsList);

		Assert.assertEquals(UIHelper.getTextContentOfList(assetDetailsPage.assetCustodyList), assetCustodyLabelsList);

	}

	@Test
	public void C44796() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check for buttons in Bound Asset details page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);
		String mfgName = "MFG" + Utils.generateRandomNumber();
		String warehouseLocation = "WHL" + Utils.generateRandomNumber();
		String maintenanceDate = Utils.addDaysToDate(MAINTENANCE_DATE_FORMAT, 7);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost, mfgName, warehouseLocation,
				maintenanceDate);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		if (manageAssetsPage.bindButton.isEnabled()) {
			manageAssetsPage.bindButton.click();
		}

		manageAssetsPage.bindAssetWithSensor(sensorID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);

		pageNavigations.navigateToAssetsPage();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "MONITORED");

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.clickAssetDetailsLink();
		commonPage.waitUntilLoaderIconNotVisible();
		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);

		ArrayList<String> assetDetailsButtonsLabelsList = new ArrayList<String>(
				Arrays.asList("unbind", "edit", "check out"));
		
		Assert.assertTrue(UIHelper.getTextContentOfList(assetDetailsPage.assetDetailsButtonsLabelsList).containsAll(assetDetailsButtonsLabelsList));

	}

	@Test
	public void C44797() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest(
				"Check if chain of custody keeps updating when transfer, checkin, checkout, service of asset is done");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);
		String mfgName = "MFG" + Utils.generateRandomNumber();
		String warehouseLocation = "WHL" + Utils.generateRandomNumber();
		String maintenanceDate = Utils.addDaysToDate(MAINTENANCE_DATE_FORMAT, 7);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost, mfgName, warehouseLocation,
				maintenanceDate);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		if (manageAssetsPage.bindButton.isEnabled()) {
			manageAssetsPage.bindButton.click();
		}

		manageAssetsPage.bindAssetWithSensor(sensorID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "MONITORED");

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		String trmUserName = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE,
				"digitalTwinUserName");

		String ftUserName = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE,
				"fieldTechUserName");

		if (manageAssetsPage.checkoutButton.isEnabled()) {
			manageAssetsPage.checkoutButton.click();

		}

		UIHelper.waitUntilElement(manageAssetsPage.fieldTechSearchBar);
		UIHelper.enterCharByChar(manageAssetsPage.fieldTechSearchBar, ftUserName);
		UIHelper.waitUntilElement(commonPage.searchResult);
		commonPage.searchResult.click();
		manageAssetsPage.enterButton.click();
		UIHelper.waitUntilElement(manageAssetsPage.checkoutConfirmButton);
		manageAssetsPage.checkoutConfirmButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		UIHelper.waitUntilElement(commonPage.bannerMessageContainer);

		Assert.assertEquals(commonPage.getBannerMessageContent(), "Checkout success\n" + assetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		manageAssetsPage.clickAssetDetailsLink();
		commonPage.waitUntilLoaderIconNotVisible();

		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);

		Assert.assertTrue(
				UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1).contains("Check Out"));

		Assert.assertEquals(UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(2),
				"from " + trmUserName + " to " + ftUserName);

	}

	@Test
	public void C44800() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check for asset location in Asset details page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);
		String mfgName = "MFG" + Utils.generateRandomNumber();
		String warehouseLocation = "WHL" + Utils.generateRandomNumber();
		String maintenanceDate = Utils.addDaysToDate(MAINTENANCE_DATE_FORMAT, 7);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost, mfgName, warehouseLocation,
				maintenanceDate);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		if (manageAssetsPage.bindButton.isEnabled()) {
			manageAssetsPage.bindButton.click();
		}

		manageAssetsPage.bindAssetWithSensor(sensorID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "MONITORED");

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		String ftUserName = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE,
				"fieldTechUserName");

		if (manageAssetsPage.checkoutButton.isEnabled()) {
			manageAssetsPage.checkoutButton.click();

		}

		UIHelper.waitUntilElement(manageAssetsPage.fieldTechSearchBar);
		UIHelper.enterCharByChar(manageAssetsPage.fieldTechSearchBar, ftUserName);
		UIHelper.waitUntilElement(commonPage.searchResult);
		commonPage.searchResult.click();
		manageAssetsPage.enterButton.click();
		UIHelper.waitUntilElement(manageAssetsPage.checkoutConfirmButton);
		manageAssetsPage.checkoutConfirmButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		UIHelper.waitUntilElement(commonPage.bannerMessageContainer);

		Assert.assertEquals(commonPage.getBannerMessageContent(), "Checkout success\n" + assetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		manageAssetsPage.clickAssetDetailsLink();
		commonPage.waitUntilLoaderIconNotVisible();

		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);

		Assert.assertTrue(UIHelper.isElementDisplayed(assetDetailsPage.locationLabel), "Location label is not displayed");

	}

	@Test
	public void C44801() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check for latitude and longitude in Asset Details page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);
		String mfgName = "MFG" + Utils.generateRandomNumber();
		String warehouseLocation = "WHL" + Utils.generateRandomNumber();
		String maintenanceDate = Utils.addDaysToDate(MAINTENANCE_DATE_FORMAT, 7);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost, mfgName, warehouseLocation,
				maintenanceDate);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();
		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.clickAssetDetailsLink();
		commonPage.waitUntilLoaderIconNotVisible();

		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);
		
		Assert.assertTrue(UIHelper.isElementDisplayed(assetDetailsPage.latitudeLabel), "Latitude label is not displayed");
		
		Assert.assertTrue(UIHelper.isElementDisplayed(assetDetailsPage.longitudeLabel), "Longitude label is not displayed");


	}

	@Test
	public void C44802() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check if check out button is enabled for newly added asset");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);
		String mfgName = "MFG" + Utils.generateRandomNumber();
		String warehouseLocation = "WHL" + Utils.generateRandomNumber();
		String maintenanceDate = Utils.addDaysToDate(MAINTENANCE_DATE_FORMAT, 7);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost, mfgName, warehouseLocation,
				maintenanceDate);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		if (manageAssetsPage.bindButton.isEnabled()) {
			manageAssetsPage.bindButton.click();
		}

		manageAssetsPage.bindAssetWithSensor(sensorID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "MONITORED");

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		Assert.assertTrue(UIHelper.isElementDisplayed(manageAssetsPage.checkoutButton));

	}

	@Test
	public void C44803() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check if asset can be assigned to field technician after checking out");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);
		String mfgName = "MFG" + Utils.generateRandomNumber();
		String warehouseLocation = "WHL" + Utils.generateRandomNumber();
		String maintenanceDate = Utils.addDaysToDate(MAINTENANCE_DATE_FORMAT, 7);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost, mfgName, warehouseLocation,
				maintenanceDate);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		if (manageAssetsPage.bindButton.isEnabled()) {
			manageAssetsPage.bindButton.click();
		}

		manageAssetsPage.bindAssetWithSensor(sensorID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "MONITORED");

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		if (manageAssetsPage.checkoutButton.isEnabled()) {
			manageAssetsPage.checkoutButton.click();

		}

		Assert.assertTrue(UIHelper.isElementDisplayed(manageAssetsPage.fieldTechSearchBar));

	}

	@Test
	public void C44804() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest(
				"Check if all buttons are disabled when asset is in �In Transfer� state Only Bind/Unbind button is enabled");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToSensorsPage();

		String sensorID = "Sensor" + Utils.generateRandomNumber();

		sensorsPage.createSensor(sensorID);

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String qrCode = "AssetQR" + uniqueID;
		String assetSKUCode = "assetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(9999);
		String mfgName = "MFG" + Utils.generateRandomNumber();
		String warehouseLocation = "WHL" + Utils.generateRandomNumber();
		String maintenanceDate = Utils.addDaysToDate(MAINTENANCE_DATE_FORMAT, 7);

		manageAssetsPage.createAsset(assetID, qrCode, assetSKUCode, assetCost, mfgName, warehouseLocation,
				maintenanceDate);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		if (manageAssetsPage.bindButton.isEnabled()) {
			manageAssetsPage.bindButton.click();
		}

		manageAssetsPage.bindAssetWithSensor(sensorID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(5).getText().trim(), "MONITORED");

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		String ftUserName = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE,
				"fieldTechUserName");

		if (manageAssetsPage.checkoutButton.isEnabled()) {
			manageAssetsPage.checkoutButton.click();

		}

		UIHelper.waitUntilElement(manageAssetsPage.fieldTechSearchBar);
		UIHelper.enterCharByChar(manageAssetsPage.fieldTechSearchBar, ftUserName);
		UIHelper.waitUntilElement(commonPage.searchResult);
		commonPage.searchResult.click();
		manageAssetsPage.enterButton.click();
		UIHelper.waitUntilElement(manageAssetsPage.checkoutConfirmButton);
		manageAssetsPage.checkoutConfirmButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		UIHelper.waitUntilElement(commonPage.bannerMessageContainer);

		Assert.assertEquals(commonPage.getBannerMessageContent(), "Checkout success\n" + assetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsListData().get(4), "IN_TRANSFER");

		Assert.assertTrue(UIHelper.getListOfElements(manageAssetsPage.unbindButtonsList).size() == 2);

		Assert.assertTrue(UIHelper.getListOfElements(manageAssetsPage.unbindButtonsList).get(0).isEnabled());
	}

	@Test
	public void C44805() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check if all buttons are disabled when asset is in �In Possession� state \r\n"
				+ "Only Bind/Unbind button is enabled");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String assetName = "Asset" + uniqueID;
		String assetType = "fb0037bb-7ebb-4eaa-b4ee-9957e9197a7b";
		String assetSKU = "AssetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(999);
		String assetManufacturer = "AssetMFG";
		String assetCondition = "Good";
		String warehouseLocation = "LOC";
		String maintenanceDate = Utils.getCurrentTimeStamp();
		String assetQRCode = "AssetQR" + uniqueID;

		String createdAssetID = restAPIUtility.createAsset(assetID, assetName, assetType, assetSKU, assetCost,
				assetManufacturer, assetCondition, warehouseLocation, maintenanceDate, assetQRCode);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		Assert.assertEquals(commonPage.getColumnsList().get(1).getText().trim(), assetID);

		manageAssetsPage.getCheckBoxesList().get(1).click();

		UIHelper.pressBackspaceKey(manageAssetsPage.searchBar);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		String ftUserName = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE,
				"fieldTechUserName");

		if (manageAssetsPage.checkoutButton.isEnabled()) {
			manageAssetsPage.checkoutButton.click();

		}

		UIHelper.waitUntilElement(manageAssetsPage.fieldTechSearchBar);
		UIHelper.enterCharByChar(manageAssetsPage.fieldTechSearchBar, ftUserName);
		UIHelper.waitUntilElement(commonPage.searchResult);
		commonPage.searchResult.click();
		manageAssetsPage.enterButton.click();
		UIHelper.waitUntilElement(manageAssetsPage.checkoutConfirmButton);
		manageAssetsPage.checkoutConfirmButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getBannerMessageContent(), "Checkout success\n" + assetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);
		Thread.sleep(2000);

		manageAssetsPage.getCheckBoxesList().get(1).click();
		Thread.sleep(2000);

		Assert.assertEquals(commonPage.getColumnsListData().get(4), "IN_TRANSFER");

		String FD_AccessToken = RestAPIUtility.getAccessToken(
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechUserName"),
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechPassword"));

		/**** Accept Asset as FT ****/

		String acceptedAssetID = restAPIUtility.acceptAsset(createdAssetID, FD_AccessToken, "true");

		Assert.assertEquals(createdAssetID, acceptedAssetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.assetDetailsLink.click();

		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);
		
		for (int i = 0; i < 15; i++) {
			Thread.sleep(5000);
			driver.navigate().refresh();
			commonPage.waitUntilLoaderIconNotVisible();
			UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);
			if (!UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1).contains("In Possession")) {
				Thread.sleep(2000);
				driver.navigate().refresh();
				commonPage.waitUntilLoaderIconNotVisible();
			} else {
				break;
			}
		}

		Assert.assertTrue(
				UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1).contains("In Possession"));

		Assert.assertTrue(commonPage.bindButton.isEnabled());

		Assert.assertTrue(commonPage.editButton.isEnabled());

	}

	@Test
	public void C44806() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check if user is able to checkin asset which is returned");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String assetName = "Asset" + uniqueID;
		String assetType = "fb0037bb-7ebb-4eaa-b4ee-9957e9197a7b";
		String assetSKU = "AssetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(999);
		String assetManufacturer = "AssetMFG";
		String assetCondition = "Good";
		String warehouseLocation = "LOC";
		String maintenanceDate = Utils.getCurrentTimeStamp();
		String assetQRCode = "AssetQR" + uniqueID;

		String createdAssetID = restAPIUtility.createAsset(assetID, assetName, assetType, assetSKU, assetCost,
				assetManufacturer, assetCondition, warehouseLocation, maintenanceDate, assetQRCode);

		String userID = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE,
				"fieldTechUserName");

		String FD_AccessToken = RestAPIUtility.getAccessToken(
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechUserName"),
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechPassword"));

		restAPIUtility.checkoutAsset(createdAssetID, userID);

		String acceptedAssetID = restAPIUtility.acceptAsset(createdAssetID, FD_AccessToken, "true");

		Assert.assertEquals(createdAssetID, acceptedAssetID);

		String returnedAssetID = restAPIUtility.returnAsset(acceptedAssetID, FD_AccessToken);

		Assert.assertEquals(createdAssetID, returnedAssetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.assetDetailsLink.click();

		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);

		Assert.assertTrue(UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1).contains("Return"));

		Assert.assertTrue(commonPage.bindButton.isEnabled());

		Assert.assertTrue(commonPage.editButton.isEnabled());

		Assert.assertTrue(assetDetailsPage.checkinButton.isEnabled());

		assetDetailsPage.checkinButton.click();

		manageAssetsPage.selectAssetGoodCondition();

		manageAssetsPage.enterButton.click();

		manageAssetsPage.checkoutConfirmButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getBannerMessageContent(), "Check in success\n" + assetID);

	}

	@Test
	public void C44807() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check if user is able to move asset to In Service");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String assetName = "Asset" + uniqueID;
		String assetType = "fb0037bb-7ebb-4eaa-b4ee-9957e9197a7b";
		String assetSKU = "AssetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(999);
		String assetManufacturer = "AssetMFG";
		String assetCondition = "Good";
		String warehouseLocation = "LOC";
		String maintenanceDate = Utils.getCurrentTimeStamp();
		String assetQRCode = "AssetQR" + uniqueID;

		String createdAssetID = restAPIUtility.createAsset(assetID, assetName, assetType, assetSKU, assetCost,
				assetManufacturer, assetCondition, warehouseLocation, maintenanceDate, assetQRCode);

		String userID = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE,
				"fieldTechUserName");

		String FD_AccessToken = RestAPIUtility.getAccessToken(
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechUserName"),
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechPassword"));

		restAPIUtility.checkoutAsset(createdAssetID, userID);

		String acceptedAssetID = restAPIUtility.acceptAsset(createdAssetID, FD_AccessToken, "true");

		Assert.assertEquals(createdAssetID, acceptedAssetID);

		String returnedAssetID = restAPIUtility.returnAsset(acceptedAssetID, FD_AccessToken);

		Assert.assertEquals(createdAssetID, returnedAssetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.assetDetailsLink.click();

		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);

		Assert.assertTrue(UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1).contains("Return"));

		Assert.assertTrue(commonPage.bindButton.isEnabled());

		Assert.assertTrue(commonPage.editButton.isEnabled());

		Assert.assertTrue(assetDetailsPage.checkinButton.isEnabled());

		assetDetailsPage.checkinButton.click();

		manageAssetsPage.selectAssetGoodCondition();

		manageAssetsPage.enterButton.click();

		manageAssetsPage.checkoutConfirmButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getBannerMessageContent(), "Check in success\n" + assetID);

		if (assetDetailsPage.serviceButton.isEnabled()) {
			assetDetailsPage.serviceButton.click();
		}

		UIHelper.waitUntilElement(manageAssetsPage.fieldTechSearchBar);
		UIHelper.enterCharByChar(manageAssetsPage.fieldTechSearchBar, userID);
		UIHelper.waitUntilElement(commonPage.searchResult);
		commonPage.searchResult.click();
		manageAssetsPage.enterButton.click();
		UIHelper.waitUntilElement(manageAssetsPage.checkoutConfirmButton);
		manageAssetsPage.checkoutConfirmButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getBannerMessageContent(), "Transfer in service success\n" + assetID);

		Assert.assertTrue(
				UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1).contains("Service"));

	}

	@Test
	public void C44808() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check if user is able to move asset to In Service   -> In Posession");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String assetName = "Asset" + uniqueID;
		String assetType = "fb0037bb-7ebb-4eaa-b4ee-9957e9197a7b";
		String assetSKU = "AssetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(999);
		String assetManufacturer = "AssetMFG";
		String assetCondition = "Good";
		String warehouseLocation = "LOC";
		String maintenanceDate = Utils.getCurrentTimeStamp();
		String assetQRCode = "AssetQR" + uniqueID;

		String createdAssetID = restAPIUtility.createAsset(assetID, assetName, assetType, assetSKU, assetCost,
				assetManufacturer, assetCondition, warehouseLocation, maintenanceDate, assetQRCode);

		String userID = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE,
				"fieldTechUserName");

		String FD_AccessToken = RestAPIUtility.getAccessToken(
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechUserName"),
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechPassword"));

		restAPIUtility.checkoutAsset(createdAssetID, userID);

		String acceptedAssetID = restAPIUtility.acceptAsset(createdAssetID, FD_AccessToken, "true");

		Assert.assertEquals(createdAssetID, acceptedAssetID);

		String returnedAssetID = restAPIUtility.returnAsset(acceptedAssetID, FD_AccessToken);

		Assert.assertEquals(createdAssetID, returnedAssetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.assetDetailsLink.click();

		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);

		Assert.assertTrue(UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1).contains("Return"));

		Assert.assertTrue(commonPage.bindButton.isEnabled());

		Assert.assertTrue(commonPage.editButton.isEnabled());

		Assert.assertTrue(assetDetailsPage.checkinButton.isEnabled());

		assetDetailsPage.checkinButton.click();

		manageAssetsPage.selectAssetGoodCondition();

		manageAssetsPage.enterButton.click();

		manageAssetsPage.checkoutConfirmButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getBannerMessageContent(), "Check in success\n" + assetID);

		if (assetDetailsPage.serviceButton.isEnabled()) {
			assetDetailsPage.serviceButton.click();
		}

		UIHelper.waitUntilElement(manageAssetsPage.fieldTechSearchBar);
		UIHelper.enterCharByChar(manageAssetsPage.fieldTechSearchBar, userID);
		UIHelper.waitUntilElement(commonPage.searchResult);
		commonPage.searchResult.click();
		manageAssetsPage.enterButton.click();
		UIHelper.waitUntilElement(manageAssetsPage.checkoutConfirmButton);
		manageAssetsPage.checkoutConfirmButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getBannerMessageContent(), "Transfer in service success\n" + assetID);

		for (int i = 0; i < 15; i++) {
			Thread.sleep(5000);
			driver.navigate().refresh();
			commonPage.waitUntilLoaderIconNotVisible();
			UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);
			if (!UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1).contains("Service")) {
				Thread.sleep(2000);
				driver.navigate().refresh();
				commonPage.waitUntilLoaderIconNotVisible();
			} else {
				break;
			}
		}

		Assert.assertTrue(
				UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1).contains("Service"));

		acceptedAssetID = restAPIUtility.acceptAsset(createdAssetID, FD_AccessToken, "true");

		Assert.assertEquals(createdAssetID, acceptedAssetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		for (int i = 0; i < 15; i++) {
			Thread.sleep(5000);
			driver.navigate().refresh();
			commonPage.waitUntilLoaderIconNotVisible();
			UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);
			if (!UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1).contains("In Possession")) {
				Thread.sleep(2000);
				driver.navigate().refresh();
				commonPage.waitUntilLoaderIconNotVisible();
			} else {
				break;
			}
		}

		System.out.println(UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1));

		Assert.assertTrue(
				UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1).contains("In Possession"));

	}

	@Test
	public void C44809() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check if condition is updated when field technician selects damaged");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String assetName = "Asset" + uniqueID;
		String assetType = "fb0037bb-7ebb-4eaa-b4ee-9957e9197a7b";
		String assetSKU = "AssetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(999);
		String assetManufacturer = "AssetMFG";
		String assetCondition = "Good";
		String warehouseLocation = "LOC";
		String maintenanceDate = Utils.getCurrentTimeStamp();
		String assetQRCode = "AssetQR" + uniqueID;

		String createdAssetID = restAPIUtility.createAsset(assetID, assetName, assetType, assetSKU, assetCost,
				assetManufacturer, assetCondition, warehouseLocation, maintenanceDate, assetQRCode);

		String userID = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE,
				"fieldTechUserName");

		String FD_AccessToken = RestAPIUtility.getAccessToken(
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechUserName"),
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechPassword"));

		restAPIUtility.checkoutAsset(createdAssetID, userID);

		String acceptedAssetID = restAPIUtility.acceptAsset(createdAssetID, FD_AccessToken, "false");

		Assert.assertEquals(createdAssetID, acceptedAssetID);

		String returnedAssetID = restAPIUtility.returnAsset(acceptedAssetID, FD_AccessToken);

		Assert.assertEquals(createdAssetID, returnedAssetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.assetDetailsLink.click();

		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);

		assetDetailsPage.checkinButton.click();

		manageAssetsPage.selectAssetDamagedCondition();

		manageAssetsPage.enterButton.click();

		manageAssetsPage.checkoutConfirmButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getBannerMessageContent(), "Check in success\n" + assetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(UIHelper.getElementText(assetDetailsPage.assetConditionLabel), "Damaged");

	}

	@Test
	public void C44810() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check if condition is updated when field technician selects good");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String assetName = "Asset" + uniqueID;
		String assetType = "fb0037bb-7ebb-4eaa-b4ee-9957e9197a7b";
		String assetSKU = "AssetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(999);
		String assetManufacturer = "AssetMFG";
		String assetCondition = "Good";
		String warehouseLocation = "LOC";
		String maintenanceDate = Utils.getCurrentTimeStamp();
		String assetQRCode = "AssetQR" + uniqueID;

		String createdAssetID = restAPIUtility.createAsset(assetID, assetName, assetType, assetSKU, assetCost,
				assetManufacturer, assetCondition, warehouseLocation, maintenanceDate, assetQRCode);

		String userID = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE,
				"fieldTechUserName");

		String FD_AccessToken = RestAPIUtility.getAccessToken(
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechUserName"),
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechPassword"));

		restAPIUtility.checkoutAsset(createdAssetID, userID);

		String acceptedAssetID = restAPIUtility.acceptAsset(createdAssetID, FD_AccessToken, "true");

		Assert.assertEquals(createdAssetID, acceptedAssetID);

		String returnedAssetID = restAPIUtility.returnAsset(acceptedAssetID, FD_AccessToken);

		Assert.assertEquals(createdAssetID, returnedAssetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.assetDetailsLink.click();

		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);

		assetDetailsPage.checkinButton.click();

		manageAssetsPage.selectAssetGoodCondition();

		manageAssetsPage.enterButton.click();

		manageAssetsPage.checkoutConfirmButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getBannerMessageContent(), "Check in success\n" + assetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(UIHelper.getElementText(assetDetailsPage.assetConditionLabel), "Good");

	}

	@Test
	public void C44811() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check if condition is updated when national tool room manager selects damaged");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String assetName = "Asset" + uniqueID;
		String assetType = "fb0037bb-7ebb-4eaa-b4ee-9957e9197a7b";
		String assetSKU = "AssetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(999);
		String assetManufacturer = "AssetMFG";
		String assetCondition = "Good";
		String warehouseLocation = "LOC";
		String maintenanceDate = Utils.getCurrentTimeStamp();
		String assetQRCode = "AssetQR" + uniqueID;

		String createdAssetID = restAPIUtility.createAsset(assetID, assetName, assetType, assetSKU, assetCost,
				assetManufacturer, assetCondition, warehouseLocation, maintenanceDate, assetQRCode);

		String userID = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE,
				"fieldTechUserName");

		String FD_AccessToken = RestAPIUtility.getAccessToken(
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechUserName"),
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechPassword"));

		restAPIUtility.checkoutAsset(createdAssetID, userID);

		String acceptedAssetID = restAPIUtility.acceptAsset(createdAssetID, FD_AccessToken, "true");

		Assert.assertEquals(createdAssetID, acceptedAssetID);

		String returnedAssetID = restAPIUtility.returnAsset(acceptedAssetID, FD_AccessToken);

		Assert.assertEquals(createdAssetID, returnedAssetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.assetDetailsLink.click();

		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);

		Assert.assertTrue(UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1).contains("Return"));

		Assert.assertTrue(commonPage.bindButton.isEnabled());

		Assert.assertTrue(commonPage.editButton.isEnabled());

		Assert.assertTrue(assetDetailsPage.checkinButton.isEnabled());

		assetDetailsPage.checkinButton.click();

		manageAssetsPage.selectAssetDamagedCondition();

		manageAssetsPage.enterButton.click();

		manageAssetsPage.checkoutConfirmButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getBannerMessageContent(), "Check in success\n" + assetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(UIHelper.getElementText(assetDetailsPage.assetConditionLabel), "Damaged");

	}

	@Test
	public void C44812() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Check if condition is updated when national tool room manager selects good");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String assetName = "Asset" + uniqueID;
		String assetType = "fb0037bb-7ebb-4eaa-b4ee-9957e9197a7b";
		String assetSKU = "AssetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(999);
		String assetManufacturer = "AssetMFG";
		String assetCondition = "Good";
		String warehouseLocation = "LOC";
		String maintenanceDate = Utils.getCurrentTimeStamp();
		String assetQRCode = "AssetQR" + uniqueID;

		String createdAssetID = restAPIUtility.createAsset(assetID, assetName, assetType, assetSKU, assetCost,
				assetManufacturer, assetCondition, warehouseLocation, maintenanceDate, assetQRCode);

		String userID = PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE,
				"fieldTechUserName");

		String FD_AccessToken = RestAPIUtility.getAccessToken(
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechUserName"),
				PropertyFileUtils.getPropValuesFromConfig(PropertyFileUtils.WEB_PROPERTIES_FILE, "fieldTechPassword"));

		restAPIUtility.checkoutAsset(createdAssetID, userID);

		String acceptedAssetID = restAPIUtility.acceptAsset(createdAssetID, FD_AccessToken, "true");

		Assert.assertEquals(createdAssetID, acceptedAssetID);

		String returnedAssetID = restAPIUtility.returnAsset(acceptedAssetID, FD_AccessToken);

		Assert.assertEquals(createdAssetID, returnedAssetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.assetDetailsLink.click();

		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);

		Assert.assertTrue(UIHelper.getTextContentOfList(assetDetailsPage.custodyActionsList).get(1).contains("Return"));

		Assert.assertTrue(commonPage.bindButton.isEnabled());

		Assert.assertTrue(commonPage.editButton.isEnabled());

		Assert.assertTrue(assetDetailsPage.checkinButton.isEnabled());

		assetDetailsPage.checkinButton.click();

		manageAssetsPage.selectAssetGoodCondition();

		manageAssetsPage.enterButton.click();

		manageAssetsPage.checkoutConfirmButton.click();

		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(commonPage.getBannerMessageContent(), "Check in success\n" + assetID);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		Assert.assertEquals(UIHelper.getElementText(assetDetailsPage.assetConditionLabel), "Good");

	}

	@Test
	public void C44813() throws IOException, InterruptedException, CloudLeafException {

		test = extent.startTest("Edit asset name from asset detail page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToAssetsPage();

		String uniqueID = Utils.generateRandomNumber();

		String assetID = "Asset" + uniqueID;
		String assetName = "Asset" + uniqueID;
		String assetType = "fb0037bb-7ebb-4eaa-b4ee-9957e9197a7b";
		String assetSKU = "AssetSKU" + uniqueID;
		int assetCost = Utils.generateRandomNumberInRange(999);
		String assetManufacturer = "AssetMFG";
		String assetCondition = "Good";
		String warehouseLocation = "LOC";
		String maintenanceDate = Utils.getCurrentTimeStamp();
		String assetQRCode = "AssetQR" + uniqueID;

		restAPIUtility.createAsset(assetID, assetName, assetType, assetSKU, assetCost, assetManufacturer,
				assetCondition, warehouseLocation, maintenanceDate, assetQRCode);

		driver.navigate().refresh();
		commonPage.waitUntilLoaderIconNotVisible();

		manageAssetsPage.enterSearchText(assetID);

		commonPage.getColumnsList().get(0).click();

		UIHelper.waitUntilElement(manageAssetsPage.assetDetailsLink);

		manageAssetsPage.assetDetailsLink.click();

		UIHelper.waitUntilElement(assetDetailsPage.chainOfCustodyWidget);

	}
}
