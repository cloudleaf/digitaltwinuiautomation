package com.cloudleaf.webautomation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.cloudleaf.automation.base.BasePage;
import com.cloudleaf.automation.base.UIHelper;
import com.cloudleaf.automation.exceptions.CloudLeafException;
import com.cloudleaf.automation.utils.Utils;

public class TestMappingAdminPage extends BasePage {

	@Test
	public void C44691() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate Sites are displayed in mapping admin for National Toolroom Manager");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToMappingAdminPage();

		Assert.assertTrue(mappingAdminPage.searchBar.isDisplayed());

		Assert.assertTrue(UIHelper.getListOfElements(mappingAdminPage.locationsList).size() > 0,
				" Locations list is not displayed");
	}

	@Test
	public void C44694() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate search box is present in the list pane");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToMappingAdminPage();

		Assert.assertTrue(mappingAdminPage.searchBar.isDisplayed());
	}

	@Test
	public void C44695() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate search box functionality in the list pane");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToMappingAdminPage();

		String locationName = "Location" + Utils.generateRandomNumber();
		String pinCode = "500008";
		mappingAdminPage.createLocation(locationName, pinCode);

		UIHelper.waitUntilElement(mappingAdminPage.bannerMessageContainer);
		Assert.assertEquals(mappingAdminPage.bannerMessageContainer.getText(), "Location created\n" + locationName);

		mappingAdminPage.enterSearchText(locationName);

		mappingAdminPage.searchResult.click();

		Assert.assertEquals(mappingAdminPage.summaryPaneLocationName.getText(), locationName);
	}

	@Test
	public void C44696() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate X icon is present in the summary pane in mapping admin page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToMappingAdminPage();

		String locationName = "Location" + Utils.generateRandomNumber();
		String pinCode = "500008";
		mappingAdminPage.createLocation(locationName, pinCode);

		UIHelper.waitUntilElement(mappingAdminPage.bannerMessageContainer);
		Assert.assertEquals(mappingAdminPage.bannerMessageContainer.getText(), "Location created\n" + locationName);

		mappingAdminPage.enterSearchText(locationName);

		UIHelper.waitUntilElement(mappingAdminPage.searchResult);

		mappingAdminPage.searchResult.click();

		Assert.assertEquals(mappingAdminPage.summaryPaneLocationName.getText(), locationName);

		Assert.assertFalse(mappingAdminPage.summaryPaneCloseIcon.isDisplayed());
	}

	@Test
	public void C44697() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate X icon functionality in summary pane");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToMappingAdminPage();

		String locationName = "Location" + Utils.generateRandomNumber();
		String pinCode = "500008";
		mappingAdminPage.createLocation(locationName, pinCode);

		UIHelper.waitUntilElement(mappingAdminPage.bannerMessageContainer);
		Assert.assertEquals(mappingAdminPage.bannerMessageContainer.getText(), "Location created\n" + locationName);

		mappingAdminPage.enterSearchText(locationName);

		UIHelper.waitUntilElement(mappingAdminPage.searchResult);

		mappingAdminPage.searchResult.click();

		Assert.assertEquals(mappingAdminPage.summaryPaneLocationName.getText(), locationName);

		mappingAdminPage.summaryPaneCloseIcon.click();
		Thread.sleep(2000);

		Assert.assertFalse(mappingAdminPage.summaryPaneLocationName.isDisplayed());
	}

	@Test
	public void C44698() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate Map is present in the mapping admin page");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToMappingAdminPage();

		UIHelper.waitUntilElement(mappingAdminPage.zoomInButton);

		Assert.assertTrue(mappingAdminPage.map.isDisplayed(), "Map is not displayed");

		Assert.assertTrue(mappingAdminPage.zoomInButton.isDisplayed(), "Zoom In button is not displayed");

		Assert.assertTrue(mappingAdminPage.zoomOutButton.isDisplayed(), "Zoom Out button is not displayed");
	}

	@Test(enabled = true)
	public void C44700() throws IOException, InterruptedException, CloudLeafException {
		test = extent
				.startTest("Validate Site details pane has vertical scroll to display the areas count greater than 50");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToMappingAdminPage();

		manageAssetsPage.loadNext50ItemsLink.click();
		commonPage.waitUntilLoaderIconNotVisible();

		List<WebElement> listOfLocations = driver.findElements(mappingAdminPage.locationsList);

		for (int i = 0; i < listOfLocations.size(); i++) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", listOfLocations.get(i));
			Thread.sleep(500);
		}

	}

	@Test
	public void C44703() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate Address field is removed in the map while adding a new Site");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToMappingAdminPage();

		String locationName = "Location" + Utils.generateRandomNumber();
		String pinCode = "500008";
		mappingAdminPage.createLocation(locationName, pinCode);

		UIHelper.waitUntilElement(mappingAdminPage.bannerMessageContainer);
		Assert.assertEquals(mappingAdminPage.bannerMessageContainer.getText(), "Location created\n" + locationName);

		mappingAdminPage.enterSearchText(locationName);

		UIHelper.waitUntilElement(mappingAdminPage.searchResult);

		mappingAdminPage.searchResult.click();

		Assert.assertEquals(mappingAdminPage.summaryPaneLocationName.getText(), locationName);

		Assert.assertTrue(UIHelper.isElementNotExists(mappingAdminPage.summaryPaneAddressDetails));
	}

	@Test
	public void C44704() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate Address field is present in the add Site pane");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToMappingAdminPage();

		mappingAdminPage.clickAddButton();

		Assert.assertTrue(mappingAdminPage.addressField.isDisplayed(), "Address field is not displayed");

	}

	@Test
	public void C44705() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate latitude and longitude are displayed in site details pane");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToMappingAdminPage();

		String locationName = "Location" + Utils.generateRandomNumber();
		String pinCode = "500008";
		mappingAdminPage.createLocation(locationName, pinCode);

		UIHelper.waitUntilElement(mappingAdminPage.bannerMessageContainer);
		Assert.assertEquals(mappingAdminPage.bannerMessageContainer.getText(), "Location created\n" + locationName);

		mappingAdminPage.enterSearchText(locationName);

		UIHelper.waitUntilElement(mappingAdminPage.searchResult);

		mappingAdminPage.searchResult.click();

		Assert.assertEquals(mappingAdminPage.summaryPaneLocationName.getText(), locationName);

		ArrayList<String> expectedLabelsList = new ArrayList<String>(
				Arrays.asList("Address:", "Latitude:", "Longitude:"));

		Assert.assertTrue(
				UIHelper.getTextContentOfList(mappingAdminPage.summaryPaneLablesList).containsAll(expectedLabelsList));
	}

	@Test
	public void C44706() throws IOException, InterruptedException, CloudLeafException {
		test = extent.startTest("Validate user cannot create a duplicate Site");

		System.out.println("Logging into the digital twin application");

		loginPage.loginDigitalTwin(digitalTwinUserName, digitalTwinPassword);

		UIHelper.waitUntilElement(loginPage.userProfileIcon);

		Assert.assertTrue(dtHomePage.isClientLogoDisplayed(), "Expected client logo not displayed");

		pageNavigations.navigateToMappingAdminPage();

		String locationName = "Location" + Utils.generateRandomNumber();
		String pinCode = "500008";
		mappingAdminPage.createLocation(locationName, pinCode);

		UIHelper.waitUntilElement(mappingAdminPage.bannerMessageContainer);
		Assert.assertEquals(mappingAdminPage.bannerMessageContainer.getText(), "Location created\n" + locationName);

		mappingAdminPage.clickAddButton();
		mappingAdminPage.enterLocationName(locationName);

		UIHelper.waitUntilElement(mappingAdminPage.alertMessage);

		Assert.assertEquals(mappingAdminPage.alertMessage.getText().trim(),
				"Location with this name exists, please choose a unique name.");
	}

}
